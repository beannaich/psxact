use crate::{
  timing::timing_add_cpu_time,
  util::{events::EventCollector, fifo::Fifo},
  AddressWidth, InterruptType, IO,
};

use super::{
  device::{Device, DeviceDsr},
  host_device::HostDevice,
};

pub struct InputBaud {
  counter: i32,
  factor: i32,
  reload: i32,
}

impl InputBaud {
  pub fn new() -> InputBaud {
    InputBaud {
      counter: 0x0088,
      factor: 1,
      reload: 0x0088,
    }
  }
}

pub struct InputDsr {
  level: DeviceDsr,
  interrupt_enable: i32,
}

impl InputDsr {
  pub fn new() -> InputDsr {
    InputDsr {
      level: DeviceDsr::High,
      interrupt_enable: 0,
    }
  }
}

pub struct InputRx {
  fifo: Fifo<u8, 3>,
  enable: bool,
  interrupt_enable: bool,
  interrupt_mode: i32,
  buffer: u8,
}

impl InputRx {
  pub fn new() -> InputRx {
    InputRx {
      fifo: Fifo::new(),
      enable: false,
      interrupt_enable: false,
      interrupt_mode: 0,
      buffer: 0,
    }
  }
}

pub struct InputTx {
  buffer: u8,
  enable: bool,
  pending: bool,
  interrupt_enable: bool,
}

impl InputTx {
  pub fn new() -> InputTx {
    InputTx {
      buffer: 0,
      enable: false,
      pending: false,
      interrupt_enable: false,
    }
  }
}

pub struct InputPort {
  memcard: [Device; 2],
  control: [Device; 2],
  output: i32,
  select: i32,
}

impl InputPort {
  pub fn new() -> InputPort {
    InputPort {
      memcard: [Device::not_connected(), Device::not_connected()],
      control: [Device::digital_pad(), Device::not_connected()],
      output: 0,
      select: 0,
    }
  }
}

pub struct InputCore {
  baud: InputBaud,
  dsr: InputDsr,
  rx: InputRx,
  tx: InputTx,
  port: InputPort,

  bit: i32,
  interrupt: bool,
}

impl InputCore {
  pub fn new() -> InputCore {
    InputCore {
      baud: InputBaud::new(),
      dsr: InputDsr::new(),
      rx: InputRx::new(),
      tx: InputTx::new(),
      port: InputPort::new(),
      bit: 0,
      interrupt: false,
    }
  }

  pub fn latch(&mut self, device1: &HostDevice, device2: &HostDevice) {
    self.port.memcard[0].latch(device1);
    self.port.memcard[1].latch(device2);
    self.port.control[0].latch(device1);
    self.port.control[1].latch(device2);
  }

  pub fn tick(&mut self, amount: i32, events: &mut EventCollector) {
    self.baud.counter -= amount;

    while self.baud.counter <= 0 {
      self.baud.counter += self.baud.reload * self.baud.factor;

      // clock output shifter

      if self.tx.enable && self.tx.pending {
        let tx_bit = (self.tx.buffer >> self.bit) & 1;
        self.tx.buffer |= 1 << self.bit;

        let rx_bit = self.port.control[0].send(tx_bit)
          & self.port.memcard[0].send(tx_bit)
          & self.port.control[1].send(tx_bit)
          & self.port.memcard[1].send(tx_bit);

        self.rx.buffer &= !(1 << self.bit);
        self.rx.buffer |= rx_bit << self.bit;

        self.bit += 1;

        if self.bit == 8 {
          self.bit = 0;
          self.tx.pending = false;

          self.write_rx(self.rx.buffer, events);
        }
      }
    }

    self.dsr.level = self.port.control[0].tick(amount, DeviceDsr::High);
    self.dsr.level = self.port.memcard[0].tick(amount, self.dsr.level);
    self.dsr.level = self.port.control[1].tick(amount, self.dsr.level);
    self.dsr.level = self.port.memcard[1].tick(amount, self.dsr.level);

    if self.dsr.level == DeviceDsr::Low {
      self.send_interrupt(events);
    }
  }

  pub fn write_rx(&mut self, data: u8, events: &mut EventCollector) {
    self.rx.fifo.write(data);

    if self.rx.interrupt_enable {
      self.send_interrupt(events);
    }
  }

  pub fn send_interrupt(&mut self, events: &mut EventCollector) {
    if !self.interrupt {
      self.interrupt = true;

      events.set_irq(InterruptType::Input);
    }
  }
}

impl IO for InputCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if width == AddressWidth::Byte && address == 0x1F80_1040 {
      return if self.rx.fifo.is_empty() {
        0xFF
      } else {
        self.rx.fifo.read() as u32
      };
    }

    if width == AddressWidth::Word || width == AddressWidth::Half {
      match address {
        0x1F80_1044 => {
          let data = (if self.tx.pending { 0 } else { 1 }) | //   0     TX Ready Flag 1   (1=Ready/Started)
            ((!self.rx.fifo.is_empty() as u32) << 1) |
            (1 << 2) | //   2     TX Ready Flag 2   (1=Ready/Finished)
            (0 << 3) | //   3     RX Parity Error   (0=No, 1=Error; Wrong Parity, when enabled)  (sticky)
            ((self.dsr.level as u32) << 7) |
            ((self.interrupt as u32) << 9) |
            ((self.baud.counter as u32) << 11);

          return data as u32;
        }
        0x1F80_104A => {
          let data = ((self.tx.enable as i32) << 0)
            | (self.port.output << 1)
            | ((self.rx.enable as i32) << 2)
            | (self.rx.interrupt_mode << 8)
            | ((self.tx.interrupt_enable as i32) << 10)
            | ((self.rx.interrupt_enable as i32) << 11)
            | (self.dsr.interrupt_enable << 12)
            | (self.port.select << 13);

          return (data as u16) as u32;
        }
        0x1F80_104E => return (self.baud.reload as u16) as u32,
        _ => {
          panic!("")
        }
      }
    }

    panic!("")
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    timing_add_cpu_time(4);

    if width == AddressWidth::Byte && address == 0x1F80_1040 {
      self.tx.buffer = val as u8;
      self.tx.pending = true;
      return;
    }

    if width == AddressWidth::Word || width == AddressWidth::Half {
      match address {
        0x1F80_1048 => {
          // 1F801048h JOY_MODE (R/W) (usually 000Dh, ie. 8bit, no parity, MUL1)
          //
          //   0-1   Baudrate Reload Factor (1=MUL1, 2=MUL16, 3=MUL64) (or 0=MUL1, too)
          //   2-3   Character Length       (0=5bits, 1=6bits, 2=7bits, 3=8bits)
          //   4     Parity Enable          (0=No, 1=Enable)
          //   5     Parity Type            (0=Even, 1=Odd) (seems to be vice-versa...?)
          //   6-7   Unknown (always zero)
          //   8     CLK Output Polarity    (0=Normal:High=Idle, 1=Inverse:Low=Idle)
          //   9-15  Unknown (always zero)

          if val != 0x000D {
            panic!("1048 - non-standard value: {:04x}", val);
          }
          return;
        }
        0x1F80_104A => {
          if (val & 0x40) != 0 {
            self.interrupt = false;
            self.port.output = 0;
            self.port.select = 0;
            self.rx.fifo.clear();
          } else {
            if (val & 0x10) != 0 {
              self.interrupt = false;
            }

            self.tx.enable = ((val >> 0) & 1) != 0;
            self.port.output = ((val >> 1) & 1) as i32;
            self.rx.enable = ((val >> 2) & 1) != 0;
            self.rx.interrupt_mode = ((val >> 8) & 3) as i32;
            self.tx.interrupt_enable = ((val >> 10) & 1) != 0;
            self.rx.interrupt_enable = ((val >> 11) & 1) != 0;
            self.dsr.interrupt_enable = ((val >> 12) & 1) as i32;
            self.port.select = ((val >> 13) & 1) as i32;

            self.port.control[0].set_dtr(self.port.output == 1 && self.port.select == 0);
            self.port.memcard[0].set_dtr(self.port.output == 1 && self.port.select == 0);
            self.port.control[1].set_dtr(self.port.output == 1 && self.port.select == 1);
            self.port.memcard[1].set_dtr(self.port.output == 1 && self.port.select == 1);
          }
          return;
        }
        0x1F80_104E => {
          self.baud.reload = (val as u16) as i32;
          return;
        }
        _ => {
          panic!("")
        }
      }
    }

    panic!("")
  }
}
