#[derive(Clone, Copy)]
pub enum HostDeviceButton {
  Released = 0,
  Pressed = 1,
}

impl Default for HostDeviceButton {
  fn default() -> Self {
    Self::Released
  }
}

impl Into<HostDeviceButton> for bool {
  fn into(self) -> HostDeviceButton {
    if self {
      HostDeviceButton::Pressed
    } else {
      HostDeviceButton::Released
    }
  }
}

/*
///   .----.                .----.
///   | LB |________________| RB |
///   | LF \________________/ RF |
///  /                            \
/// /   U   [SELECT] [START]   3   \
/// |  L R  ________________  1 2  |
///  \  D  /                \  0  /
///   |   |                  |   |
///    \_/                    \_/
*/

#[derive(Default)]
pub struct HostDevice {
  pub dpad_down: HostDeviceButton,
  pub dpad_left: HostDeviceButton,
  pub dpad_right: HostDeviceButton,
  pub dpad_up: HostDeviceButton,
  pub left_back_shoulder: HostDeviceButton,
  pub left_front_shoulder: HostDeviceButton,
  pub right_back_shoulder: HostDeviceButton,
  pub right_front_shoulder: HostDeviceButton,
  pub select: HostDeviceButton,
  pub start: HostDeviceButton,
  pub button_0: HostDeviceButton,
  pub button_1: HostDeviceButton,
  pub button_2: HostDeviceButton,
  pub button_3: HostDeviceButton,
}
