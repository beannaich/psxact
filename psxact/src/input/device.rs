use super::host_device::HostDevice;

#[derive(Clone, Copy, PartialEq)]
pub enum DeviceDsr {
  High = 0,
  Low = 1,
}

const DSR_DELAY_PERIOD: i32 = 100;
const DSR_PULSE_PERIOD: i32 = 100;

pub enum Device {
  Unconnected,
  DigitalPad {
    dsr_pending: bool,
    dsr_cycles: i32,
    state: DigitalPad,
  },
}

impl Device {
  pub fn not_connected() -> Device {
    Device::Unconnected
  }

  pub fn digital_pad() -> Device {
    Device::DigitalPad {
      dsr_pending: false,
      dsr_cycles: 0,
      state: DigitalPad::new(),
    }
  }

  pub fn start_dsr_pulse(&mut self) {
    match self {
      Device::Unconnected => (),
      Device::DigitalPad {
        dsr_pending, dsr_cycles, ..
      } => {
        *dsr_pending = true;
        *dsr_cycles = 0;
      }
    }
  }

  pub fn tick(&mut self, amount: i32, dsr: DeviceDsr) -> DeviceDsr {
    match self {
      Device::Unconnected => (),
      Device::DigitalPad {
        dsr_cycles, dsr_pending, ..
      } => {
        if *dsr_pending {
          *dsr_cycles += amount;

          if *dsr_cycles >= (DSR_DELAY_PERIOD + DSR_PULSE_PERIOD) {
            *dsr_pending = false;
          } else if *dsr_cycles >= DSR_DELAY_PERIOD {
            return DeviceDsr::Low;
          }
        }
      }
    }

    return dsr;
  }

  pub fn latch(&mut self, device: &HostDevice) {
    match self {
      Device::Unconnected => (),
      Device::DigitalPad { state, .. } => state.latch(device),
    }
  }

  pub fn send(&mut self, request: u8) -> u8 {
    match self {
      Device::Unconnected => 1,
      Device::DigitalPad { state, .. } => match state.send(request) {
        (false, tx) => tx,
        (true, tx) => {
          self.start_dsr_pulse();
          tx
        }
      },
    }
  }

  pub fn set_dtr(&mut self, next_dtr: bool) {
    match self {
      Device::Unconnected => (),
      Device::DigitalPad { state, .. } => state.set_dtr(next_dtr),
    }
  }
}

pub struct DigitalPad {
  bit: i32,
  step: i32,
  bits: u16,
  rx_buffer: u8,
  tx_buffer: u8,
  dtr: bool,
}

impl DigitalPad {
  pub fn new() -> DigitalPad {
    DigitalPad {
      bit: 0,
      step: 0,
      bits: 0,
      rx_buffer: 0,
      tx_buffer: 0xFF,
      dtr: false,
    }
  }

  pub fn latch(&mut self, device: &HostDevice) {
    self.bits = ((device.select as u16)               <<  0) |
      (0                                    <<  1) | // 1   L3/Joy-button ; analog mode only
      (0                                    <<  2) | // 2   R3/Joy-button ; analog mode only
      ((device.start as u16)                <<  3) |
      ((device.dpad_up as u16)              <<  4) |
      ((device.dpad_right as u16)           <<  5) |
      ((device.dpad_down as u16)            <<  6) |
      ((device.dpad_left as u16)            <<  7) |
      ((device.left_back_shoulder as u16)   <<  8) |
      ((device.right_back_shoulder as u16)  <<  9) |
      ((device.left_front_shoulder as u16)  << 10) |
      ((device.right_front_shoulder as u16) << 11) |
      ((device.button_3 as u16)             << 12) |
      ((device.button_2 as u16)             << 13) |
      ((device.button_0 as u16)             << 14) |
      ((device.button_1 as u16)             << 15);

    self.bits ^= 0xFFFF;
  }

  pub fn send(&mut self, data: u8) -> (bool, u8) {
    if !self.dtr {
      return (false, 1);
    }

    let tx_bit = (self.tx_buffer >> self.bit) & 1;
    let mut dsr = false;

    self.tx_buffer |= 1 << self.bit;

    self.rx_buffer &= !(1 << self.bit);
    self.rx_buffer |= data << self.bit;

    self.bit += 1;

    if self.bit == 8 {
      self.bit = 0;

      match self.step {
        0 => {
          if self.rx_buffer == 0x01 {
            dsr = true;
            self.tx_buffer = 0x41;
            self.step += 1;
          }
        }
        1 => {
          if self.rx_buffer == 0x42 {
            dsr = true;
            self.tx_buffer = 0x5A;
            self.step += 1;
          }
        }
        2 => {
          dsr = true;
          self.tx_buffer = (self.bits >> 0) as u8;
          self.step += 1;
        }
        3 => {
          dsr = true;
          self.tx_buffer = (self.bits >> 8) as u8;
          self.step += 1;
        }
        _ => (),
      }
    }

    (dsr, tx_bit)
  }

  pub fn set_dtr(&mut self, next_dtr: bool) {
    if !self.dtr && next_dtr {
      self.step = 0;
      self.bit = 0;
      self.rx_buffer = 0;
      self.tx_buffer = 0xFF;
    }

    self.dtr = next_dtr;
  }
}
