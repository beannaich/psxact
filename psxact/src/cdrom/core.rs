use std::{
  collections::BTreeMap,
  fs::{File, OpenOptions},
  io::{Read, Seek, SeekFrom},
};

use crate::{
  cdrom::{sector::*, xa_adpcm_decoder::*},
  timing::{timing_add_cpu_time, CPU_FREQ},
  util::{bcd::bcd_to_dec, events::EventCollector, fifo::Fifo},
  AddressWidth, Comms, InterruptType, IO,
};

#[derive(Clone, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct CdromTimecode {
  minute: u8,
  second: u8,
  sector: u8,
}

impl CdromTimecode {
  pub fn new(minute: u8, second: u8, sector: u8) -> CdromTimecode {
    CdromTimecode { minute, second, sector }
  }

  pub fn next(&mut self) {
    self.sector += 1;
    if self.sector == 75 {
      self.sector = 0;
      self.second += 1;
      if self.second == 60 {
        self.second = 0;
        self.minute += 1;
        // TODO: Overflow minute?
      }
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn derived_ord_works() {
    let data = vec![(1, 0, 0), (0, 1, 0), (0, 0, 1)];

    for (minute, second, sector) in data {
      let a = CdromTimecode::new(minute, second, sector);
      let b = CdromTimecode::new(0, 0, 0);
      assert!(a > b);
    }
  }
}

impl CdromTimecode {
  pub fn offset(&self) -> u64 {
    const SECTORS_PER_SECOND: u64 = 75;
    const SECONDS_PER_MINUTE: u64 = 60;
    const SECTORS_PER_MINUTE: u64 = SECTORS_PER_SECOND * SECONDS_PER_MINUTE;

    let minute = self.minute as u64 * SECTORS_PER_MINUTE;
    let second = self.second as u64 * SECTORS_PER_SECOND;
    let sector = self.sector as u64;

    let leadin = 2 * SECTORS_PER_SECOND;
    let offset = minute + second + sector;

    (offset - leadin) * (CDROM_SECTOR_SIZE as u64)
  }
}

pub enum CdromDriveState {
  Idle,
  Reading,
  Seeking,
  Playing,
}

pub struct CdromCore {
  drive_state: CdromDriveState,

  parameter: Fifo<u8, 4>,
  response: Fifo<u8, 4>,
  command: u8,
  command_pending: bool,

  mode: CdromMode,
  filter: CdromSectorFilter,
  sector: CdromSector,
  sector_read_cursor: usize,
  sector_read_offset: usize,
  sector_read_length: usize,
  sector_read_active: bool,

  index: u8,
  irq_flag: u8,
  irq_mask: u8,
  timer: i32,

  int1: Option<fn(&mut CdromCore, &mut Disc, &mut XaAdpcmDecoder)>,
  int2: Option<fn(&mut CdromCore, &mut Disc)>,

  int1_timer: i32,
  int2_timer: i32,

  seek_timecode: CdromTimecode,
  read_timecode: CdromTimecode,
  seek_pending: bool,
}

impl CdromCore {
  pub fn new() -> CdromCore {
    CdromCore {
      drive_state: CdromDriveState::Idle,
      parameter: Fifo::new(),
      response: Fifo::new(),
      command: 0,
      command_pending: false,
      mode: CdromMode::default(),
      filter: CdromSectorFilter::default(),
      sector: CdromSector::new(),
      sector_read_cursor: 0,
      sector_read_offset: 0,
      sector_read_length: 0,
      sector_read_active: false,
      index: 0,
      irq_flag: 0,
      irq_mask: 0,
      timer: 0,
      int1: None,
      int2: None,
      int1_timer: 0,
      int2_timer: 0,
      seek_timecode: CdromTimecode::default(),
      read_timecode: CdromTimecode::default(),
      seek_pending: false,
    }
  }

  pub fn clear_parameter_fifo(&mut self) {
    self.parameter.clear();
  }

  pub fn clear_response_fifo(&mut self) {
    self.response.clear();
  }

  pub fn ack_irq_flag(&mut self, val: u8) {
    if (val & 0x40) != 0 {
      self.parameter.clear();
    }

    self.set_irq_flag(self.irq_flag & !(val & 31));
  }

  pub fn get_data(&mut self) -> u8 {
    assert!(self.sector_read_active, "sector read while not active");

    if self.sector_read_active {
      assert!(
        self.sector_read_cursor < self.sector_read_length,
        "sector read beyond the end"
      );

      let result = self.sector.get(self.sector_read_cursor + self.sector_read_offset);
      self.sector_read_cursor += 1;

      if self.sector_read_cursor == self.sector_read_length {
        self.sector_read_active = false;
      }

      return result;
    }

    self.sector.get((self.sector_read_cursor + self.sector_read_offset) & !7)
  }

  pub fn get_status(&mut self) -> u8 {
    let bit2 = 0; // 2   ADPBUSY XA-ADPCM fifo empty  (0=Empty) ;set when playing XA-ADPCM sound
    let bit3 = self.parameter.is_empty() as u8;
    let bit4 = !self.parameter.is_full() as u8;
    let bit5 = !self.response.is_empty() as u8;
    let bit6 = (self.sector_read_cursor < self.sector_read_length) as u8;
    let bit7 = 0; // 7   BUSYSTS Command/parameter transmission busy  (1=Busy)

    let stat = self.index | (bit2 << 2) | (bit3 << 3) | (bit4 << 4) | (bit5 << 5) | (bit6 << 6) | (bit7 << 7);

    return stat;
  }

  pub fn set_host_control(&mut self, val: u8) {
    if !self.sector_read_active && (val & 0x80) != 0 {
      self.sector_read_cursor = 0;
    }

    self.sector_read_active = (val & 0x80) != 0;
  }

  pub fn set_irq_flag(&mut self, val: u8) {
    self.irq_flag = val & 31;
  }

  pub fn set_irq_mask(&mut self, val: u8) {
    self.irq_mask = val & 31;
  }

  pub fn set_read_time(&mut self) -> i32 {
    if self.mode.double_speed() {
      return CPU_FREQ / 150;
    } else {
      return CPU_FREQ / 75;
    }
  }

  pub fn get_seek_time(&mut self) -> i32 {
    let minute_diff = ((self.seek_timecode.minute as i32) - (self.read_timecode.minute as i32)).abs();
    let second_diff = ((self.seek_timecode.second as i32) - (self.read_timecode.second as i32)).abs();
    let sector_diff = ((self.seek_timecode.sector as i32) - (self.read_timecode.sector as i32)).abs();

    // For now, we_ll assume 100 cycles per sector to seek.
    // I_ve just pulled this number out of thin air, because why not?

    return 100 * ((minute_diff * 60) + (second_diff * 75) + sector_diff);
  }

  pub fn get_drive_status(&mut self) -> u8 {
    let mut response = 0;

    match self.drive_state {
      CdromDriveState::Idle => (),
      CdromDriveState::Reading => response |= 1 << 5,
      CdromDriveState::Seeking => response |= 1 << 6,
      CdromDriveState::Playing => response |= 1 << 7,
    }

    // 0  Error         Invalid Command/parameters (followed by Error Byte)
    // 1  Spindle Motor (0=Motor off, or in spin-up phase, 1=Motor on)
    // 2  SeekError     (0=Okay, 1=Seek error)     (followed by Error Byte)
    // 3  IdError       (0=Okay, 1=GetID denied) (also set when Setcdrom_mode_Bit4=&Mode1)
    // 4  ShellOpen     Once shell open (0=Closed, 1=Is/was Open)

    response |= 1 << 1;

    return response;
  }

  pub fn get_irq_flag(&mut self) -> u8 {
    0xE0 | self.irq_flag
  }

  pub fn get_irq_mask(&mut self) -> u8 {
    0xE0 | self.irq_mask
  }

  pub fn get_parameter(&mut self) -> u8 {
    self.parameter.read()
  }

  pub fn get_response(&mut self) -> u8 {
    self.response.read()
  }

  pub fn set_command(&mut self, val: u8) {
    assert!(!self.command_pending, "Command written before previous command was started.");

    self.command = val;
    self.command_pending = true;
  }

  pub fn set_parameter(&mut self, val: u8) {
    self.parameter.write(val);
  }

  pub fn set_response(&mut self, val: u8) {
    self.response.write(val);
  }

  pub fn try_deliver_sector_as_adpcm(&mut self, xa: &mut XaAdpcmDecoder) -> bool {
    if self.sector.get_mode() != 2 {
      return false;
    }

    if !self.mode.send_xa_adpcm_to_spu() {
      return false;
    }

    if self.mode.filter_xa_adpcm() && !self.filter.is_match(&self.sector) {
      return false;
    }

    if (self.sector.get_xa_sub_mode() & 0x44) != 0x44 {
      return false;
    }

    xa.decode(&mut self.sector);

    return true;
  }

  pub fn try_deliver_sector_as_data(&mut self) -> bool {
    let sector_type = self.sector.get_type();
    if sector_type == CdromSectorType::Mode2Form1 || sector_type == CdromSectorType::Mode2Form2 {
      if self.mode.filter_xa_adpcm() && (self.sector.get_xa_sub_mode() & 0x44) == 0x44 {
        return false;
      }
    }

    if self.mode.read_whole_sector() {
      self.sector_read_offset = 12;
      self.sector_read_length = CDROM_SECTOR_SIZE - 12;
    } else {
      match sector_type {
        CdromSectorType::Unknown | CdromSectorType::Mode0 => {
          panic!("Mode 0");
        }
        CdromSectorType::Mode1 => {
          self.sector_read_offset = 16;
          self.sector_read_length = 2048;
        }
        CdromSectorType::Mode2Form1 => {
          self.sector_read_offset = 24;
          self.sector_read_length = 2048;
        }
        CdromSectorType::Mode2Form2 => {
          panic!("Mode 2 Form 2");
        }
      }
    }

    true
  }

  pub fn tick(&mut self, amount: i32, events: &mut EventCollector, disc: &mut Disc, xa: &mut XaAdpcmDecoder) {
    self.int1_timer = (self.int1_timer - amount).max(0);
    self.int2_timer = (self.int2_timer - amount).max(0);

    self.timer -= amount;

    while self.timer < 0 {
      self.timer += 3000;

      // Do nothing if there is an un-acked IRQ.
      if self.irq_flag != 0 {
        continue;
      }

      // Process a pending INT1.
      if self.int1_timer == 0 {
        if let Some(int1) = self.int1 {
          int1(self, disc, xa);
          self.int1_timer = self.set_read_time();
          continue;
        }
      }

      // Process a pending INT2.
      if self.int2_timer == 0 {
        if let Some(int2) = self.int2.take() {
          int2(self, disc);
          continue;
        }
      }

      // Process a pending command.
      if self.command_pending {
        let cmd = self.command;
        self.command_pending = false;
        self.clear_response_fifo();

        match cmd {
          0x01 => {
            self.complete_command(3);
          }
          0x02 => {
            self.seek_timecode.minute = bcd_to_dec(self.get_parameter());
            self.seek_timecode.second = bcd_to_dec(self.get_parameter());
            self.seek_timecode.sector = bcd_to_dec(self.get_parameter());
            self.seek_pending = true;

            self.complete_command(3);
          }
          0x03 => {
            self.clear_parameter_fifo();

            self.complete_command(3);
          }
          0x1B | 0x06 => {
            self.drive_state = CdromDriveState::Reading;

            self.complete_command(3);

            self.int1 = Some(CdromCore::int1_read_n);
            self.int1_timer = self.set_read_time();

            if self.seek_pending {
              self.seek_pending = false;
              self.read_timecode = self.seek_timecode.clone();
              self.int1_timer += self.get_seek_time();
            }
          }
          0x08 => {
            self.complete_command(3);

            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_stop);
            self.int2_timer = INT2_STOP_TIMING;
          }
          0x09 => {
            self.complete_command(3);

            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_pause);
            self.int2_timer = INT2_PAUSE_TIMING;
          }
          0x0A => {
            self.complete_command(3);

            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_init);
            self.int2_timer = INT2_INIT_TIMING;
          }
          0x0B => {
            self.complete_command(3);
          }
          0x0C => {
            self.complete_command(3);
          }
          0x0D => {
            let file = self.get_parameter();
            let channel = self.get_parameter();

            self.filter.set_file(file);
            self.filter.set_channel(channel);

            self.complete_command(3);
          }
          0x0E => {
            self.mode = CdromMode::new(self.get_parameter());

            self.complete_command(3);
          }
          0x10 => {
            self.set_response(self.sector.get_minute());
            self.set_response(self.sector.get_second());
            self.set_response(self.sector.get_second());
            self.set_response(self.sector.get_mode());
            self.set_response(self.sector.get_xa_file());
            self.set_response(self.sector.get_xa_channel());
            self.set_response(self.sector.get_xa_sub_mode());
            self.set_response(self.sector.get_xa_coding_info());
            self.set_irq_flag(3);
          }
          0x11 => {
            self.set_response(0x01); // Track
            self.set_response(0x01); // Index
            self.set_response(self.sector.get_minute()); // Track-relative
            self.set_response(self.sector.get_second());
            self.set_response(self.sector.get_sector());
            self.set_response(self.sector.get_minute()); // Absolute
            self.set_response(self.sector.get_second());
            self.set_response(self.sector.get_sector());
            self.set_irq_flag(3);
          }
          0x13 => {
            self.complete_command(3);
            self.set_response(0x01);
            self.set_response(0x01);
          }
          0x14 => {
            let _track = self.get_parameter();

            self.complete_command(3);
            self.set_response(0x00);
            self.set_response(0x00);
          }
          0x15 | 0x16 => {
            self.drive_state = CdromDriveState::Seeking;

            self.complete_command(3);

            // Cancel any on-going read/play operations.
            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_seek_l);
            self.int2_timer = self.get_seek_time() + INT2_SEEK_L_TIMING;
          }
          0x19 => {
            let sub_cmd = self.get_parameter();
            match sub_cmd {
              0x04 => {
                self.complete_command(3);
              }
              0x05 => {
                self.clear_parameter_fifo();
                self.set_response(0);
                self.set_response(0);
                self.set_irq_flag(3);
              }
              0x20 => {
                self.set_response(0x98);
                self.set_response(0x06);
                self.set_response(0x10);
                self.set_response(0xC3);
                self.set_irq_flag(3);
              }
              _ => {
                panic!("Unhandled sub-command: {:02x}", sub_cmd);
              }
            }
          }
          0x1A => {
            self.drive_state = CdromDriveState::Reading;

            self.complete_command(3);

            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_get_id);
            self.int2_timer = INT2_GET_ID_TIMING;
          }
          0x1E => {
            self.drive_state = CdromDriveState::Reading;

            self.complete_command(3);

            self.int1 = None;
            self.int1_timer = 0;

            self.int2 = Some(CdromCore::int2_read_toc);
            self.int2_timer = INT2_READ_TOC_TIMING;
          }
          _ => {
            panic!("Unhandled command: {:02x}", cmd);
          }
        }

        assert!(self.parameter.is_empty(), "junk data left in the parameter fifo");
      }
    }

    if self.irq_flag != 0 && (self.irq_flag & self.irq_mask) == self.irq_flag {
      events.set_irq(InterruptType::Cdrom);
    } else {
      events.ack_irq(InterruptType::Cdrom);
    }
  }

  fn complete_command(&mut self, flag: u8) {
    let stat = self.get_drive_status();
    self.set_response(stat);
    self.set_irq_flag(flag);
  }

  pub fn int1_read_n(&mut self, disc: &mut Disc, xa: &mut XaAdpcmDecoder) {
    disc.read_sector(&self.read_timecode, &mut self.sector);

    self.read_timecode.next();

    if self.try_deliver_sector_as_adpcm(xa) {
      // TODO: Send INT1?
      return;
    }

    if self.try_deliver_sector_as_data() {
      self.complete_command(1);
      return;
    }

    // Sectors silently drop out here if they can't be delivered.
  }

  pub fn int2_get_id(&mut self, disc: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;

    let (int, res) = disc.id();

    for &val in res.iter() {
      self.set_response(val);
    }

    self.set_irq_flag(int);
  }

  pub fn int2_init(&mut self, _: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;
    self.mode = CdromMode::new(0);

    self.complete_command(2);
  }

  pub fn int2_pause(&mut self, _: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;

    self.complete_command(2);
  }

  pub fn int2_read_toc(&mut self, _: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;

    self.complete_command(2);
  }

  pub fn int2_seek_l(&mut self, _: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;

    self.read_timecode = self.seek_timecode.clone();
    self.seek_pending = false;

    self.complete_command(2);
  }

  pub fn int2_stop(&mut self, _: &mut Disc) {
    self.drive_state = CdromDriveState::Idle;

    self.complete_command(2);
  }
}

impl Comms for CdromCore {
  fn dma_speed(&mut self) -> i32 {
    40
  }

  fn dma_read_ready(&mut self) -> bool {
    true
  }

  fn dma_write_ready(&mut self) -> bool {
    true
  }

  fn dma_read(&mut self) -> u32 {
    let b0 = self.get_data() as u32;
    let b1 = self.get_data() as u32;
    let b2 = self.get_data() as u32;
    let b3 = self.get_data() as u32;

    (b3 << 24) | (b2 << 16) | (b1 << 8) | b0
  }

  fn dma_write(&mut self, _: u32) {}
}

impl IO for CdromCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    add_cpu_time(&width);

    if width != AddressWidth::Byte {
      panic!("non-byte read of cd-rom");
    }

    match address {
      0x1F80_1800 => self.get_status() as u32,
      0x1F80_1801 => self.get_response() as u32,
      0x1F80_1802 => self.get_data() as u32,
      0x1F80_1803 => {
        if (self.index & 1) == 1 {
          self.get_irq_flag() as u32
        } else {
          self.get_irq_mask() as u32
        }
      }
      _ => {
        panic!("")
      }
    }
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    add_cpu_time(&width);

    if width != AddressWidth::Byte {
      panic!("non-byte write of cd-rom")
    }

    if address == 0x1F80_1800 {
      self.index = (data & 3) as u8;
    } else {
      // I'm going to make my life easier by creating a single identifier for
      // each port, created from the index and I/O address.

      match (self.index * 4) | ((address & 3) as u8) {
        0x1 => self.set_command(data as u8),
        0x2 => self.set_parameter(data as u8),
        0x3 => self.set_host_control(data as u8),

        //  5 - Sound Map Data Out
        0x6 => self.set_irq_mask(data as u8),
        0x7 => self.ack_irq_flag(data as u8),

        //  9 - Sound Map Coding Info
        0xA => (), // 10 - Audio Volume for Left-CD-Out to Left-SPU-Input
        0xB => (), // 11 - Audio Volume for Left-CD-Out to Right-SPU-Input

        0xD => (), // 13 - Audio Volume for Right-CD-Out to Right-SPU-Input
        0xE => (), // 14 - Audio Volume for Right-CD-Out to Left-SPU-Input
        0xF => (), // 15 - Audio Volume Apply Changes
        _ => {
          panic!("")
        }
      }
    }
  }
}

fn add_cpu_time(width: &AddressWidth) {
  match width {
    AddressWidth::Byte => timing_add_cpu_time(9),
    AddressWidth::Half => timing_add_cpu_time(15),
    AddressWidth::Word => timing_add_cpu_time(27),
  }
}

pub const INT2_GET_ID_TIMING: i32 = 19_000;
pub const INT2_INIT_TIMING: i32 = 900_000;
pub const INT2_PAUSE_TIMING: i32 = 1_800; // TODO: This can take much longer?
pub const INT2_READ_TOC_TIMING: i32 = 16_000_000;
pub const INT2_SEEK_L_TIMING: i32 = 1_800;
pub const INT2_STOP_TIMING: i32 = 1_800; // TODO: This can take much longer?

pub enum Disc {
  None,
  Disc(CachedDisc),
}

impl Disc {
  pub fn none() -> Disc {
    Disc::None
  }

  pub fn disc(path: &str) -> std::io::Result<Disc> {
    let cached = CachedDisc::new(path)?;
    Ok(Disc::Disc(cached))
  }

  pub fn id(&self) -> (u8, [u8; 8]) {
    match self {
      Disc::None => {
        return (5, [0x08, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]);
      }
      Disc::Disc(_) => {
        return (2, [0x02, 0x00, 0x20, 0x00, b'S', b'C', b'E', b'A']);
      }
    }
  }

  pub fn read_sector(&mut self, timecode: &CdromTimecode, sector: &mut CdromSector) {
    match self {
      Disc::None => {
        panic!("attempt to read sector from empty disc");
      }
      Disc::Disc(disc) => disc.read_sector(timecode, sector),
    }
  }
}

pub struct CachedDisc {
  file: File,
  indices: BTreeMap<CdromTimecode, usize>,
  sectors: Vec<CdromSector>,
}

impl CachedDisc {
  pub fn new(path: &str) -> std::io::Result<CachedDisc> {
    let file = OpenOptions::new().read(true).open(path)?;

    let mut sectors = Vec::with_capacity(75);
    for _ in 0..75 {
      sectors.push(CdromSector::new());
    }

    Ok(CachedDisc {
      file,
      indices: BTreeMap::new(),
      sectors,
    })
  }

  fn read_sector(&mut self, timecode: &CdromTimecode, sector: &mut CdromSector) {
    if !self.indices.contains_key(timecode) {
      self.indices.clear();

      let mut timecode = timecode.clone();

      for idx in 0..75 {
        self.indices.insert(timecode.clone(), idx);

        let offset = timecode.offset();

        self.file.seek(SeekFrom::Start(offset as u64)).unwrap();
        self.file.read_exact(&mut self.sectors[idx].buffer).unwrap();

        // Sanity check to make sure we've read the correct sector.

        assert!(
          bcd_to_dec(self.sectors[idx].get_minute()) == timecode.minute
            && bcd_to_dec(self.sectors[idx].get_second()) == timecode.second
            && bcd_to_dec(self.sectors[idx].get_sector()) == timecode.sector
        );

        timecode.next();
      }
    }

    let idx = self.indices[timecode];
    let src = &self.sectors[idx];

    sector.buffer.copy_from_slice(&src.buffer)
  }
}
