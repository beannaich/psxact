pub const CDROM_SECTOR_SIZE: usize = 0x930;

#[derive(PartialEq)]
pub enum CdromSectorType {
  /// Used when a determination couldn't be made.
  Unknown,
  /// Empty, used for padding?
  Mode0,
  /// CD-ROM, used for data.
  Mode1,
  /// CD-XA Form 1, used for data.
  Mode2Form1,
  /// CD-XA Form 2, used for XA-ADPCM.
  Mode2Form2,
}

pub struct CdromSector {
  pub buffer: [u8; CDROM_SECTOR_SIZE],
}

impl CdromSector {
  pub fn new() -> CdromSector {
    CdromSector {
      buffer: [0; CDROM_SECTOR_SIZE],
    }
  }

  /// Returns the uninterpreted minute marker from the current data.
  #[inline]
  pub fn get_minute(&self) -> u8 {
    self.get(MINUTE_OFFSET)
  }

  /// Returns the uninterpreted second marker from the current data.
  #[inline]
  pub fn get_second(&self) -> u8 {
    self.get(SECOND_OFFSET)
  }

  /// Returns the uninterpreted sector marker from the current data.
  #[inline]
  pub fn get_sector(&self) -> u8 {
    self.get(SECTOR_OFFSET)
  }

  /// Returns the uninterpreted mode marker from the current data.
  #[inline]
  pub fn get_mode(&self) -> u8 {
    self.get(MODE_OFFSET)
  }

  /// Returns the uninterpreted CD-XA file marker from the current data.
  #[inline]
  pub fn get_xa_file(&self) -> u8 {
    self.get(XA_FILE_OFFSET)
  }

  /// Returns the uninterpreted CD-XA channel marker from the current data.
  #[inline]
  pub fn get_xa_channel(&self) -> u8 {
    self.get(XA_CHANNEL_OFFSET)
  }

  /// Returns the uninterpreted CD-XA sub-mode marker from the current data.
  #[inline]
  pub fn get_xa_sub_mode(&self) -> u8 {
    self.get(XA_SUB_MODE_OFFSET)
  }

  /// Returns the uninterpreted CD-XA coding-info marker from the current data.
  #[inline]
  pub fn get_xa_coding_info(&self) -> u8 {
    self.get(XA_CODING_INFO_OFFSET)
  }

  /// Returns the sector type for the currently loaded data.
  pub fn get_type(&self) -> CdromSectorType {
    match self.get_mode() {
      0 => CdromSectorType::Mode0,
      1 => CdromSectorType::Mode1,
      2 => {
        if (self.get_xa_sub_mode() & 0x20) != 0 {
          CdromSectorType::Mode2Form2
        } else {
          CdromSectorType::Mode2Form1
        }
      }
      _ => CdromSectorType::Unknown,
    }
  }

  #[inline]
  pub fn get(&self, index: usize) -> u8 {
    assert!(index < CDROM_SECTOR_SIZE, "unable to get sector data. index={}", index);

    self.buffer[index]
  }
}

const MINUTE_OFFSET: usize = 12;
const SECOND_OFFSET: usize = 13;
const SECTOR_OFFSET: usize = 14;
const MODE_OFFSET: usize = 15;
const XA_FILE_OFFSET: usize = 16;
const XA_CHANNEL_OFFSET: usize = 17;
const XA_SUB_MODE_OFFSET: usize = 18;
const XA_CODING_INFO_OFFSET: usize = 19;

#[derive(Default)]
pub struct CdromSectorFilter {
  file: u8,
  channel: u8,
}

impl CdromSectorFilter {
  pub fn set_file(&mut self, val: u8) {
    self.file = val;
  }

  pub fn set_channel(&mut self, val: u8) {
    self.channel = val;
  }

  pub fn is_match(&self, sector: &CdromSector) -> bool {
    sector.get_type() == CdromSectorType::Mode2Form2
      && sector.get_xa_file() == self.file
      && sector.get_xa_channel() == self.channel
  }
}

#[derive(Default)]
pub struct CdromMode(u8);

impl CdromMode {
  pub fn new(val: u8) -> CdromMode {
    CdromMode(val)
  }

  #[inline]
  pub fn double_speed(&self) -> bool {
    (self.0 & 0x80) != 0
  }

  #[inline]
  pub fn send_xa_adpcm_to_spu(&self) -> bool {
    (self.0 & 0x40) != 0
  }

  #[inline]
  pub fn read_whole_sector(&self) -> bool {
    (self.0 & 0x20) != 0
  }

  #[inline]
  pub fn filter_xa_adpcm(&self) -> bool {
    (self.0 & 0x08) != 0
  }

  #[inline]
  pub fn send_audio_reports(&self) -> bool {
    (self.0 & 0x04) != 0
  }

  #[inline]
  pub fn auto_pause_audio(&self) -> bool {
    (self.0 & 0x02) != 0
  }

  #[inline]
  pub fn read_cd_da(&self) -> bool {
    (self.0 & 0x01) != 0
  }
}
