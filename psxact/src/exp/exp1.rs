use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

pub struct Expansion1;

fn add_cpu_time(width: &AddressWidth) {
  match width {
    AddressWidth::Byte => timing_add_cpu_time(8),
    AddressWidth::Half => timing_add_cpu_time(14),
    AddressWidth::Word => timing_add_cpu_time(26),
  }
}

impl IO for Expansion1 {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    add_cpu_time(&width);

    if let AddressWidth::Byte = width {
      if address == 0x1F00_0004 || address == 0x1F00_0084 {
        return 0;
      }
    }

    panic!("unhandled exp1 read. address={:04x}", address)
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    add_cpu_time(&width);

    panic!(
      "unhandled exp1 write. address={:04x}, data={:04x}",
      address, data
    )
  }
}
