use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

pub struct Expansion3;

fn add_cpu_time(width: &AddressWidth) {
  match width {
    AddressWidth::Byte => timing_add_cpu_time(7),
    AddressWidth::Half => timing_add_cpu_time(7),
    AddressWidth::Word => timing_add_cpu_time(11),
  }
}

impl IO for Expansion3 {
  fn io_read(&mut self, width: AddressWidth, _: u32) -> u32 {
    add_cpu_time(&width);
    panic!("")
  }

  fn io_write(&mut self, width: AddressWidth, _: u32, _: u32) {
    add_cpu_time(&width);
    panic!("")
  }
}
