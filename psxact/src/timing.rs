pub const NTSC_COLOR_CARRIER: f64 = 3_579_545.454545455;
pub const NTSC_SCANLINE_LENGTH: f64 = 227.5;
pub const NTSC_SCANLINE_FREQ: f64 = NTSC_COLOR_CARRIER / NTSC_SCANLINE_LENGTH;
pub const NTSC_LINES_PER_FIELD: f64 = 262.5;
pub const NTSC_LINES_PER_FRAME: f64 = 525.0;
pub const NTSC_FRAME_RATE: f64 = NTSC_SCANLINE_FREQ / NTSC_LINES_PER_FRAME;
pub const NTSC_FIELD_RATE: f64 = NTSC_SCANLINE_FREQ / NTSC_LINES_PER_FIELD;

// These are included to check that the math is correct.
#[test]
pub fn test_timing_works() {
  assert!(((NTSC_FRAME_RATE * 100.0) as i32) == 29_97);
  assert!(((NTSC_FIELD_RATE * 100.0) as i32) == 59_94);
}

pub const CPU_FREQ: i32 = 33_868_800;
pub const GPU_FREQ: i32 = 53_222_400;
pub const SPU_FREQ: i32 = 44_100;

pub const SPU_DIVIDER: i32 = CPU_FREQ / SPU_FREQ;

pub const GPU_LINE_LENGTH: f64 = 3_379.2; // GPU_FREQ / NTSC_SCANLINE_FREQ;

//

static mut CPU_TIME: i32 = 0;

pub fn timing_add_cpu_time(amount: i32) {
  unsafe {
    CPU_TIME += amount;
  }
}

pub fn timing_get_cpu_time() -> i32 {
  return 3;
}

pub fn timing_reset_cpu_time() {
  unsafe {
    CPU_TIME = 0;
  }
}
