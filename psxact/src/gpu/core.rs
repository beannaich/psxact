use crate::{
  gpu::{types::*, vram::Vram},
  timing::{timing_add_cpu_time, GPU_LINE_LENGTH},
  util::{events::EventCollector, fifo::Fifo},
  AddressWidth, Comms, InterruptType, IO,
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DisplayDepth {
  Bpp15,
  Bpp24,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum HResolution {
  H256 = 256,
  H320 = 320,
  H368 = 368,
  H512 = 512,
  H640 = 640,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum VResolution {
  V240 = 240,
  V480 = 480,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Field {
  Even,
  Odd,
}

const GP0: u32 = 0x1F80_1810;
const GP1: u32 = 0x1F80_1814;
const READ: u32 = 0x1F80_1810;
const STAT: u32 = 0x1F80_1814;

pub struct GpuTransfer {
  reg_x: i32,
  reg_y: i32,
  reg_w: i32,
  reg_h: i32,

  run_active: bool,
  run_x: i32,
  run_y: i32,
}

impl GpuTransfer {
  fn new() -> GpuTransfer {
    GpuTransfer {
      reg_x: 0,
      reg_y: 0,
      reg_w: 0,
      reg_h: 0,
      run_active: false,
      run_x: 0,
      run_y: 0,
    }
  }
}

pub struct GpuCore {
  fifo: Fifo<u32, 4>,
  vram: Vram,

  prescaler: i32,
  counter: i32,

  h_resolution: HResolution,
  v_resolution: VResolution,
  display_depth: DisplayDepth,
  field: Field,
  video_buffer: Box<[u32; 640 * 480]>,

  status: u32,

  data_latch: u32,
  texture_window_mask_x: u32,
  texture_window_mask_y: u32,
  texture_window_offset_x: u32,
  texture_window_offset_y: u32,
  drawing_area_x1: i32,
  drawing_area_y1: i32,
  drawing_area_x2: i32,
  drawing_area_y2: i32,
  x_offset: i32,
  y_offset: i32,
  display_area_x: u32,
  display_area_y: u32,
  display_area_x1: u32,
  display_area_y1: u32,
  display_area_x2: u32,
  display_area_y2: u32,
  textured_rectangle_x_flip: bool,
  textured_rectangle_y_flip: bool,

  cpu_to_transfer: GpuTransfer,
  to_cpu_transfer: GpuTransfer,
}

impl GpuCore {
  pub fn new() -> GpuCore {
    GpuCore {
      fifo: Fifo::new(),
      vram: Vram::new(),
      prescaler: 0,
      counter: 0,
      h_resolution: HResolution::H256,
      v_resolution: VResolution::V240,
      display_depth: DisplayDepth::Bpp15,
      field: Field::Even,
      video_buffer: Box::new([0; 640 * 480]),
      status: 0x1480_2000,
      data_latch: 0,
      texture_window_mask_x: 0,
      texture_window_mask_y: 0,
      texture_window_offset_x: 0,
      texture_window_offset_y: 0,
      drawing_area_x1: 0,
      drawing_area_y1: 0,
      drawing_area_x2: 0,
      drawing_area_y2: 0,
      x_offset: 0,
      y_offset: 0,
      display_area_x: 0,
      display_area_y: 0,
      display_area_x1: 0,
      display_area_y1: 0,
      display_area_x2: 0,
      display_area_y2: 0,
      textured_rectangle_x_flip: false,
      textured_rectangle_y_flip: false,
      cpu_to_transfer: GpuTransfer::new(),
      to_cpu_transfer: GpuTransfer::new(),
    }
  }

  fn vram_transfer_read(&mut self) -> u16 {
    if !self.to_cpu_transfer.run_active {
      return 0;
    }

    let val = self.vram.read_16_data(
      self.to_cpu_transfer.reg_x + self.to_cpu_transfer.run_x,
      self.to_cpu_transfer.reg_y + self.to_cpu_transfer.run_y,
    );

    let transfer = &mut self.to_cpu_transfer;
    transfer.run_x += 1;

    if transfer.run_x == transfer.reg_w {
      transfer.run_x = 0;
      transfer.run_y += 1;

      if transfer.run_y == transfer.reg_h {
        transfer.run_y = 0;
        transfer.run_active = false;
      }
    }

    val
  }

  fn vram_transfer_write(&mut self, val: u16) {
    if !self.cpu_to_transfer.run_active {
      return;
    }

    self.vram.write_16_data(
      self.cpu_to_transfer.reg_x + self.cpu_to_transfer.run_x,
      self.cpu_to_transfer.reg_y + self.cpu_to_transfer.run_y,
      val,
    );

    let transfer = &mut self.cpu_to_transfer;
    transfer.run_x += 1;

    if transfer.run_x == transfer.reg_w {
      transfer.run_x = 0;
      transfer.run_y += 1;

      if transfer.run_y == transfer.reg_h {
        transfer.run_y = 0;
        transfer.run_active = false;
      }
    }
  }

  fn draw_point(&mut self, point: &Point, color: &Color) {
    if point.x < self.drawing_area_x1
      || point.x > self.drawing_area_x2
      || point.y < self.drawing_area_y1
      || point.y > self.drawing_area_y2
    {
      return;
    }

    self.vram.write_16_data(point.x, point.y, color.dither(&point).to_u16());
  }

  fn draw_line(&mut self) {
    // panic!("Line drawing isn't implemented yet.");
  }

  fn fifo_at(&self, index: usize) -> u32 {
    self.fifo.at(index)
  }

  fn fifo_clear(&mut self) {
    self.fifo.clear();
  }

  fn fifo_size(&mut self) -> usize {
    self.fifo.size()
  }

  fn fifo_write(&mut self, val: u32) {
    self.fifo.write(val)
  }

  fn gp1(&mut self, val: u32) {
    match (val >> 24) & 0x3F {
      0x00 => {
        self.status = 0x1480_2000;
        self.to_cpu_transfer.run_active = false;
        self.textured_rectangle_x_flip = false;
        self.textured_rectangle_y_flip = false;
      }
      0x01 => {
        self.fifo_clear();
      }
      0x02 => self.status &= !(1 << 24),
      0x03 => {
        self.status &= !0x0080_0000;
        self.status |= (val << 23) & 0x0080_0000;
      }
      0x04 => {
        self.status &= !0x6000_0000;
        self.status |= (val << 29) & 0x6000_0000;
      }
      0x05 => {
        self.display_area_x = (val >> 0) & 0x3FF;
        self.display_area_y = (val >> 10) & 0x1FF;
      }
      0x06 => {
        self.display_area_x1 = (val >> 0) & 0xFFF;
        self.display_area_x2 = (val >> 12) & 0xFFF;
      }
      0x07 => {
        self.display_area_y1 = (val >> 0) & 0x3FF;
        self.display_area_y2 = (val >> 10) & 0x3FF;
      }
      0x08 => {
        if (val & (1 << 6)) != 0 {
          self.h_resolution = HResolution::H368;
        } else {
          match val & 3 {
            0 => self.h_resolution = HResolution::H256,
            1 => self.h_resolution = HResolution::H320,
            2 => self.h_resolution = HResolution::H512,
            _ => self.h_resolution = HResolution::H640,
          }
        }

        if (val & (1 << 2)) != 0 && (val & (1 << 5)) != 0 {
          self.v_resolution = VResolution::V480;
        } else {
          self.v_resolution = VResolution::V240;
        }

        if (val & (1 << 4)) != 0 {
          self.display_depth = DisplayDepth::Bpp24;
        } else {
          self.display_depth = DisplayDepth::Bpp15;
        }

        self.status &= !0x7F_4000;
        self.status |= (val << 17) & 0x7E_0000;
        self.status |= (val << 10) & 0x01_0000;
        self.status |= (val << 7) & 0x00_4000;
      }

      0x10 | 0x11 | 0x12 | 0x13 | 0x14 | 0x15 | 0x16 | 0x17 | 0x18 | 0x19 | 0x1A | 0x1B | 0x1C | 0x1D | 0x1E | 0x1F => {
        match val & 15 {
          3 => self.data_latch = ((self.drawing_area_x1 & 0x3FF) | ((self.drawing_area_y1 & 0x3FF) << 10)) as u32,
          4 => self.data_latch = ((self.drawing_area_x2 & 0x3FF) | ((self.drawing_area_y2 & 0x3FF) << 10)) as u32,
          5 => self.data_latch = ((self.x_offset & 0x7FF) | ((self.y_offset & 0x7FF) << 11)) as u32,
          7 => self.data_latch = 2,
          _ => panic!(""),
        }
      }

      _ => panic!(""),
    }
  }

  fn rect_get_x_length(&self) -> i32 {
    match (self.fifo_at(0) >> 27) & 3 {
      1 => 1,
      2 => 8,
      3 => 16,
      _ => {
        let index = if (self.fifo_at(0) & (1 << 26)) != 0 { 3 } else { 2 };
        ((self.fifo_at(index) >> 0) & 0xFFFF) as i32
      }
    }
  }

  fn rect_get_y_length(&self) -> i32 {
    match (self.fifo_at(0) >> 27) & 3 {
      1 => 1,
      2 => 8,
      3 => 16,
      _ => {
        let index = if (self.fifo_at(0) & (1 << 26)) != 0 { 3 } else { 2 };
        ((self.fifo_at(index) >> 16) & 0xFFFF) as i32
      }
    }
  }

  fn draw_color(
    &mut self,
    command: &Gp0Command,
    shade: &Color,
    point: &Point,
    coord: &TextureCoord,
    params: &TextureParams,
  ) {
    if command.is_texture_mapped() {
      let tex_color = self.get_texture_color(params, coord);
      if tex_color.is_full_transparent() {
        return;
      }

      let color = if !command.is_raw_texture() {
        tex_color.to_color().blend_mul(&shade)
      } else {
        tex_color.to_color()
      };

      if command.is_semi_transparent() && tex_color.is_semi_transparent() {
        let cur_color = self.vram.read_16_color(point.x, point.y);
        let color = match params.color_mix_mode {
          0 => color.blend_avg(&cur_color),
          1 => color.blend_add(&cur_color),
          2 => color.blend_sub(&cur_color),
          _ => color.blend_add_over_4(&cur_color),
        };

        self.draw_point(point, &color)
      } else {
        self.draw_point(point, &color)
      };
    } else {
      if command.is_semi_transparent() {
        let cur_color = self.vram.read_16_color(point.x, point.y);
        let color = match params.color_mix_mode {
          0 => shade.blend_avg(&cur_color),
          1 => shade.blend_add(&cur_color),
          2 => shade.blend_sub(&cur_color),
          _ => shade.blend_add_over_4(&cur_color),
        };

        self.draw_point(point, &color)
      } else {
        self.draw_point(point, shade)
      };
    }
  }

  fn get_texture_color_4bpp(&mut self, params: &TextureParams, coord: &TextureCoord) -> TextureColor {
    let mut val = self.vram.read_16_data(
      params.texture_page_x + ((coord.u / 4) as i32),
      params.texture_page_y + ((coord.v / 1) as i32),
    );

    val = (val >> ((coord.u & 3) * 4)) & 15;

    let pixel = self
      .vram
      .read_16_data(params.palette_page_x + (val as i32), params.palette_page_y);

    TextureColor::new(pixel)
  }

  fn get_texture_color_8bpp(&mut self, params: &TextureParams, coord: &TextureCoord) -> TextureColor {
    let mut val = self.vram.read_16_data(
      params.texture_page_x + ((coord.u / 2) as i32),
      params.texture_page_y + ((coord.v / 1) as i32),
    );

    val = (val >> ((coord.u & 1) * 8)) & 255;

    let pixel = self
      .vram
      .read_16_data(params.palette_page_x + (val as i32), params.palette_page_y);

    TextureColor::new(pixel)
  }

  fn get_texture_color_15bpp(&mut self, params: &TextureParams, coord: &TextureCoord) -> TextureColor {
    let pixel = self.vram.read_16_data(
      params.texture_page_x + (coord.u as i32),
      params.texture_page_y + (coord.v as i32),
    );

    TextureColor::new(pixel)
  }

  fn get_texture_color(&mut self, params: &TextureParams, coord: &TextureCoord) -> TextureColor {
    match params.texture_colors {
      0 => self.get_texture_color_4bpp(params, coord),
      1 => self.get_texture_color_8bpp(params, coord),
      _ => self.get_texture_color_15bpp(params, coord),
    }
  }

  fn draw_rectangle(&mut self) {
    let mut params = TextureParams {
      texture_page_x: ((self.status as i32) << 6) & 0x3C0,
      texture_page_y: ((self.status as i32) << 4) & 0x100,
      texture_colors: ((self.status as i32) >> 7) & 3,
      color_mix_mode: ((self.status as i32) >> 5) & 3,
      palette_page_x: 0,
      palette_page_y: 0,
    };

    let command = Gp0Command::new(self.fifo_at(0));

    let fifo_at_1 = self.fifo_at(1) as i32;

    let xofs = self.x_offset + { (((fifo_at_1 >> 0) & 0x7FF) ^ 0x400).wrapping_sub(0x400) };
    let yofs = self.y_offset + { (((fifo_at_1 >> 16) & 0x7FF) ^ 0x400).wrapping_sub(0x400) };

    let coord = if command.is_texture_mapped() {
      let fifo_at_2 = self.fifo_at(2);
      params.palette_page_x = ((fifo_at_2 >> 12) & 0x3F0) as i32;
      params.palette_page_y = ((fifo_at_2 >> 22) & 0x1FF) as i32;
      TextureCoord::from_u16(fifo_at_2 as u16)
    } else {
      TextureCoord::from_u16(0)
    };

    let w = self.rect_get_x_length();
    let h = self.rect_get_y_length();

    for y in 0..h {
      for x in 0..w {
        let this_coord = TextureCoord {
          u: coord.u.wrapping_add(x as u8),
          v: coord.v.wrapping_add(y as u8),
        };

        let point = Point {
          x: xofs + x,
          y: yofs + y,
        };

        let mut shade = Color::from_u24(self.fifo_at(0));

        self.draw_color(&command, &mut shade, &point, &this_coord, &params);
      }
    }
  }

  fn copy_vram_to_vram(&mut self) {}

  fn copy_wram_to_vram(&mut self) {
    assert!(self.fifo.size() == 3, "copy wram->vram with wrong parameter count");

    let fifo_at_1 = self.fifo_at(1);
    let fifo_at_2 = self.fifo_at(2);

    let transfer = &mut self.cpu_to_transfer;
    transfer.reg_x = (fifo_at_1 & 0xFFFF) as i32;
    transfer.reg_y = (fifo_at_1 >> 16) as i32;
    transfer.reg_w = (fifo_at_2 & 0xFFFF) as i32;
    transfer.reg_h = (fifo_at_2 >> 16) as i32;

    transfer.run_x = 0;
    transfer.run_y = 0;
    transfer.run_active = true;
  }

  fn copy_vram_to_wram(&mut self) {
    assert!(self.fifo.size() == 3, "copy wram->vram with wrong parameter count");

    let fifo_at_1 = self.fifo_at(1);
    let fifo_at_2 = self.fifo_at(2);

    let mut transfer = &mut self.to_cpu_transfer;
    transfer.reg_x = (fifo_at_1 & 0xFFFF) as i32;
    transfer.reg_y = (fifo_at_1 >> 16) as i32;
    transfer.reg_w = (fifo_at_2 & 0xFFFF) as i32;
    transfer.reg_h = (fifo_at_2 >> 16) as i32;

    transfer.run_x = 0;
    transfer.run_y = 0;
    transfer.run_active = true;
  }

  fn fill_rectangle(&mut self) {
    assert!(self.fifo.size() == 3, "fill rectangle with wrong parameter count");

    let fifo_at_0 = self.fifo_at(0);
    let fifo_at_1 = self.fifo_at(1) as i32;
    let fifo_at_2 = self.fifo_at(2) as i32;

    let color = Color::from_u24(fifo_at_0).to_u16();

    let start = Point {
      x: (fifo_at_1 + 0x0) & 0x3F0,
      y: (fifo_at_1 >> 16) & 0x1FF,
    };

    let count = Point {
      x: (fifo_at_2 + 0xF) & 0x7F0,
      y: (fifo_at_2 >> 16) & 0x1FF,
    };

    for y in 0..count.y {
      for x in 0..count.x {
        self.vram.write_16_data(start.x + x, start.y + y, color);
      }
    }
  }

  fn draw_triangle(&mut self, command: &Gp0Command, triangle: &Triangle) {
    let p0 = triangle.point(0);
    let p1 = triangle.point(1);
    let p2 = triangle.point(2);

    let mut min = Point {
      x: p0.x.min(p1.x.min(p2.x)),
      y: p0.y.min(p1.y.min(p2.y)),
    };

    let mut max = Point {
      x: p0.x.max(p1.x.max(p2.x)),
      y: p0.y.max(p1.y.max(p2.y)),
    };

    min.x = min.x.max(self.drawing_area_x1);
    min.y = min.y.max(self.drawing_area_y1);
    max.x = max.x.min(self.drawing_area_x2);
    max.y = max.y.min(self.drawing_area_y2);

    let dx = [p2.y - p1.y, p0.y - p2.y, p1.y - p0.y];

    let dy = [p1.x - p2.x, p2.x - p0.x, p0.x - p1.x];

    let c = [
      if dy[0] > 0 || (dy[0] == 0 && dx[0] > 0) { -1 } else { 0 },
      if dy[1] > 0 || (dy[1] == 0 && dx[1] > 0) { -1 } else { 0 },
      if dy[2] > 0 || (dy[2] == 0 && dx[2] > 0) { -1 } else { 0 },
    ];

    let mut row = [
      edge_function(&min, &p1, &p2),
      edge_function(&min, &p2, &p0),
      edge_function(&min, &p0, &p1),
    ];

    for y in min.y..=max.y {
      let mut w0 = row[0];
      row[0] += dy[0];

      let mut w1 = row[1];
      row[1] += dy[1];

      let mut w2 = row[2];
      row[2] += dy[2];

      for x in min.x..=max.x {
        if w0 > c[0] && w1 > c[1] && w2 > c[2] {
          let color = triangle.color_lerp(w0, w1, w2);
          let coord = triangle.coord_lerp(w0, w1, w2);
          let point = Point { x, y };

          self.draw_color(&command, &color, &point, &coord, &triangle.params());
        }

        w0 += dx[0];
        w1 += dx[1];
        w2 += dx[2];
      }
    }
  }

  fn draw_polygon(&mut self) {
    let command = Gp0Command::new(self.fifo_at(0));

    let num_vertices = if command.is_quad_poly() { 4 } else { 3 };
    let num_polygons = if command.is_quad_poly() { 2 } else { 1 };

    let bit1 = command.is_gouraud_shaded() as usize;
    let bit0 = command.is_texture_mapped() as usize;

    let (cf, tf, pf) = [(0, 0, 1), (0, 2, 2), (2, 0, 2), (3, 3, 3)][(bit1 << 1) | bit0];

    let mut colors = Vec::with_capacity(num_vertices);
    let mut coords = Vec::with_capacity(num_vertices);
    let mut points = Vec::with_capacity(num_vertices);

    for i in 0..num_vertices {
      let fifo0 = self.fifo_at(i * cf + 0);
      let fifo1 = self.fifo_at(i * pf + 1);
      let fifo2 = self.fifo_at(i * tf + 2);

      colors.push(Color::from_u24(fifo0));
      points.push(Point::from_u24(fifo1, self.x_offset, self.y_offset));
      coords.push(TextureCoord::from_u16(fifo2 as u16));
    }

    let params = {
      let palette = self.fifo_at(0 * tf + 2) >> 16;
      let texpage = self.fifo_at(1 * tf + 2) >> 16;

      TextureParams {
        //  11    Texture Disable (0=Normal, 1=Disable if GP1(09h).Bit0=1)   ;GPUSTAT.15
        palette_page_x: ((palette as i32) << 4) & 0x3F0,
        palette_page_y: ((palette as i32) >> 6) & 0x1FF,
        texture_colors: ((texpage as i32) >> 7) & 3,
        texture_page_x: ((texpage as i32) << 6) & 0x3C0,
        texture_page_y: ((texpage as i32) << 4) & 0x100,
        color_mix_mode: if command.is_texture_mapped() {
          (texpage >> 5) & 3
        } else {
          (self.status >> 5) & 3
        } as i32,
      }
    };

    for i in 0..num_polygons {
      let triangle = Triangle::new(&params, &points[i..], &colors[i..], &coords[i..]);

      self.draw_triangle(&command, &triangle);
    }
  }

  fn gp0(&mut self, val: u32) {
    if self.cpu_to_transfer.run_active {
      let lower = (val >> 0) as u16;
      let upper = (val >> 16) as u16;

      self.vram_transfer_write(lower);
      self.vram_transfer_write(upper);
      return;
    }

    self.fifo_write(val);

    let command = self.fifo_at(0) >> 24;

    if self.fifo_size() == COMMAND_SIZE[command as usize] as usize {
      self.run_command();
      self.fifo_clear();
    }
  }

  fn run_command(&mut self) {
    let fifo0 = self.fifo_at(0);
    let command = fifo0 >> 24;

    match command & 0xE0 {
      0x20 => self.draw_polygon(),
      0x40 => self.draw_line(),
      0x60 => self.draw_rectangle(),
      0x80 => self.copy_vram_to_vram(),
      0xA0 => self.copy_wram_to_vram(),
      0xC0 => self.copy_vram_to_wram(),
      _ => {
        match command {
          0x00 => (), // nop
          0x01 => (), // clear texture cache
          0x02 => self.fill_rectangle(),
          0x1F => self.status |= 1 << 24,
          0xE1 => {
            self.status &= !0x87FF;
            self.status |= (fifo0 << 0) & 0x7FF;
            self.status |= (fifo0 << 4) & 0x8000;

            self.textured_rectangle_x_flip = ((fifo0 >> 12) & 1) != 0;
            self.textured_rectangle_y_flip = ((fifo0 >> 13) & 1) != 0;
          }
          0xE2 => {
            self.texture_window_mask_x = (fifo0 >> 0) & 0x1F;
            self.texture_window_mask_y = (fifo0 >> 5) & 0x1F;
            self.texture_window_offset_x = (fifo0 >> 10) & 0x1F;
            self.texture_window_offset_y = (fifo0 >> 15) & 0x1F;
          }
          0xE3 => {
            assert!(
              self.fifo.size() == 1,
              "set draw area (top left) with wrong parameter count"
            );

            self.drawing_area_x1 = ((fifo0 >> 0) & 0x3FF) as i32;
            self.drawing_area_y1 = ((fifo0 >> 10) & 0x3FF) as i32;
          }
          0xE4 => {
            self.drawing_area_x2 = ((fifo0 >> 0) & 0x3FF) as i32;
            self.drawing_area_y2 = ((fifo0 >> 10) & 0x3FF) as i32;
          }
          0xE5 => {
            self.x_offset = {
              let value = fifo0 >> 0;
              (((value as i32) & 0x7FF) ^ 0x400).wrapping_sub(0x400)
            };
            self.y_offset = {
              let value = fifo0 >> 11;
              (((value as i32) & 0x7FF) ^ 0x400).wrapping_sub(0x400)
            };
          }
          0xE6 => {
            self.status &= !0x1800;
            self.status |= (fifo0 << 11) & 0x1800;
          }
          _ => {
            panic!("unhandled command. command={:08x}", command)
          }
        }
      }
    }
  }

  pub fn get_h_resolution(&self) -> HResolution {
    self.h_resolution
  }

  fn get_v_resolution(&self) -> VResolution {
    self.v_resolution
  }

  fn get_display_depth(&self) -> DisplayDepth {
    self.display_depth
  }

  pub fn get_video_buffer(&self) -> &[u32] {
    self.video_buffer.as_ref()
  }

  fn render_field(&mut self) {
    let width = self.get_h_resolution() as usize;
    let line = self.field as usize;
    let forced_blanking = (self.status & (1 << 23)) != 0;

    let mut vram_y = (self.display_area_y as i32) + (self.field as i32);
    let dy = (self.get_v_resolution() as i32) / 240;

    let mut index = match self.field {
      Field::Even => 0,
      Field::Odd => 640,
    };

    for _ in (line..480).step_by(2) {
      let mut vram_x = self.display_area_x as i32;

      for x in 0..width {
        self.video_buffer[index + x] = if forced_blanking {
          0
        } else {
          match self.get_display_depth() {
            DisplayDepth::Bpp15 => self.vram.read_16_color(vram_x, vram_y).to_u32(),
            DisplayDepth::Bpp24 => self.vram.read_24_color(vram_x, vram_y).to_u32(),
          }
        };

        vram_x += 1;
      }

      vram_y += dy;
      index += 640 * 2;
    }
  }

  fn data(&mut self) -> u32 {
    if self.to_cpu_transfer.run_active {
      let lower = self.vram_transfer_read() as u32;
      let upper = self.vram_transfer_read() as u32;
      (upper << 16) | lower
    } else {
      self.data_latch
    }
  }

  fn stat(&self) -> u32 {
    let bit27 = self.to_cpu_transfer.run_active as u32;
    let bit31 = self.field as u32;
    let bit13 = self.field as u32;

    //  26    Ready to receive Cmd Word   (0=No, 1=Ready)  ;GP0(...) ;via GP0
    //  28    Ready to receive DMA Block  (0=No, 1=Ready)  ;GP0(...) ;via GP0

    self.status | 0x1400_0000 | (bit27 << 27) | (bit31 << 31) | (bit13 << 13)
  }

  pub fn tick(&mut self, amount: i32, events: &mut EventCollector) -> bool {
    // time-base conversion
    self.prescaler += amount * 11;
    let steps = self.prescaler / 7;
    self.prescaler = self.prescaler % 7;

    // the real update function
    let vbl = self.step(steps, events);

    if (self.status & (1 << 24)) != 0 {
      events.set_irq(InterruptType::Gpu);
    } else {
      events.ack_irq(InterruptType::Gpu);
    }

    vbl
  }

  fn step(&mut self, amount: i32, events: &mut EventCollector) -> bool {
    const VBLANK_START: i32 = (241.0 * GPU_LINE_LENGTH + 0.5) as i32;
    const VBLANK_END: i32 = (262.5 * GPU_LINE_LENGTH + 0.5) as i32;

    assert!(
      amount < VBLANK_END,
      "attempting to sync gpu with more cycles than a single frame"
    );

    let prev = self.counter;
    let next = self.counter + amount;

    self.counter = next % VBLANK_END;

    if prev < VBLANK_START && next >= VBLANK_START {
      events.enter_vbl();
    }

    if prev < VBLANK_END && next >= VBLANK_END {
      events.leave_vbl();
    }

    if next >= VBLANK_END {
      self.render_field();

      if self.get_v_resolution() == VResolution::V480 {
        self.field = if self.field == Field::Even {
          Field::Odd
        } else {
          Field::Even
        };
      } else {
        self.field = Field::Even;
      }
      true
    } else {
      false
    }
  }

  pub fn start_frame(&mut self) {
    if self.get_v_resolution() == VResolution::V480 {
      // there are no unused lines to clear.
      return;
    }

    let mut index = 0;
    let pitch = 640;

    for _ in 0..240 {
      for _ in 0..640 {
        self.video_buffer[index] = 0;
        index += 1;
      }

      index += pitch;
    }
  }
}

impl Comms for GpuCore {
  fn dma_speed(&mut self) -> i32 {
    1
  }

  fn dma_read_ready(&mut self) -> bool {
    true
  }

  fn dma_write_ready(&mut self) -> bool {
    true
  }

  fn dma_read(&mut self) -> u32 {
    self.data()
  }

  fn dma_write(&mut self, val: u32) {
    self.gp0(val);
  }
}

impl IO for GpuCore {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    timing_add_cpu_time(4);

    if width != AddressWidth::Word {
      panic!("")
    }

    match address {
      READ => self.data(),
      STAT => self.stat(),
      _ => panic!(""),
    }
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    timing_add_cpu_time(4);

    if width != AddressWidth::Word {
      panic!("")
    }

    match address {
      GP0 => self.gp0(val),
      GP1 => self.gp1(val),
      _ => panic!(""),
    }
  }
}

const COMMAND_SIZE: [u8; 256] = [
  1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // $00
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // $10
  4, 4, 4, 4, 7, 7, 7, 7, 5, 5, 5, 5, 9, 9, 9, 9, // $20
  6, 6, 6, 6, 9, 9, 9, 9, 8, 8, 8, 8, 12, 12, 12, 12, // $30
  3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // $40
  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // $50
  3, 3, 3, 3, 4, 4, 4, 4, 2, 2, 2, 2, 3, 3, 3, 3, // $60
  2, 2, 2, 2, 3, 3, 3, 3, 2, 2, 2, 2, 3, 3, 3, 3, // $70
  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // $80
  4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, // $90
  3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // $a0
  3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // $b0
  3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // $c0
  3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // $d0
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // $e0
  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // $f0
];

fn edge_function(a: &Point, b: &Point, c: &Point) -> i32 {
  ((a.x - b.x) * (c.y - b.y)) - ((a.y - b.y) * (c.x - b.x))
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_gp1_08_display_mode() {
    let mut gpu = GpuCore::new();

    // Test initial values

    assert_eq!(gpu.get_h_resolution(), HResolution::H256);
    assert_eq!(gpu.get_v_resolution(), VResolution::V240);

    // Test setting horizontal resolution..

    let values = vec![
      (0x0800_0000, HResolution::H256), // ..without the "Force 368" bit.
      (0x0800_0001, HResolution::H320),
      (0x0800_0002, HResolution::H512),
      (0x0800_0003, HResolution::H640),
      (0x0800_0040, HResolution::H368), // ..with the "Force 368" bit.
      (0x0800_0041, HResolution::H368),
      (0x0800_0042, HResolution::H368),
      (0x0800_0043, HResolution::H368),
    ];

    for &(val, res) in values.iter() {
      gpu.gp1(val);
      assert_eq!(gpu.get_h_resolution(), res);
    }

    // Test setting vertical resolution..

    let values = vec![
      (0x0800_0000, VResolution::V240), // ..without the "Interlace" bit.
      (0x0800_0004, VResolution::V240),
      (0x0800_0020, VResolution::V240), // ..with the "Interlace" bit.
      (0x0800_0024, VResolution::V480),
    ];

    for &(val, res) in values.iter() {
      gpu.gp1(val);
      assert_eq!(gpu.get_v_resolution(), res);
    }

    // Test setting display depth.

    gpu.gp1(0x0800_0000);
    assert_eq!(gpu.get_display_depth(), DisplayDepth::Bpp15);

    gpu.gp1(0x0800_0010);
    assert_eq!(gpu.get_display_depth(), DisplayDepth::Bpp24);
  }

  #[test]
  fn test_status_bits() {
    let mut gpu = GpuCore::new();

    let values = vec![
      (0x0000_0000, 0xFFFF_FFFF, 0x1480_2000), // Reset
      (0x0800_0080, 0x0000_4000, 0x0000_4000), // 14    "Reverseflag"         (0=Normal, 1=Distorted)             ;GP1(08h).7
      (0x0800_0040, 0x0001_0000, 0x0001_0000), // 16    Horizontal Resolution 2     (0=256/320/512/640, 1=368)    ;GP1(08h).6
      (0x0800_0003, 0x0006_0000, 0x0006_0000), // 17-18 Horizontal Resolution 1     (0=256, 1=320, 2=512, 3=640)  ;GP1(08h).0-1
      (0x0800_0004, 0x0008_0000, 0x0008_0000), // 19    Vertical Resolution         (0=240, 1=480, when Bit22=1)  ;GP1(08h).2
      (0x0800_0008, 0x0010_0000, 0x0010_0000), // 20    Video Mode                  (0=NTSC/60Hz, 1=PAL/50Hz)     ;GP1(08h).3
      (0x0800_0010, 0x0020_0000, 0x0020_0000), // 21    Display Area Color Depth    (0=15bit, 1=24bit)            ;GP1(08h).4
      (0x0800_0020, 0x0040_0000, 0x0040_0000), // 22    Vertical Interlace          (0=Off, 1=On)                 ;GP1(08h).5
      (0x0300_0001, 0x0080_0000, 0x0080_0000), // 23    Display Enable              (0=Enabled, 1=Disabled)       ;GP1(03h).0
      (0x0200_0000, 0x0100_0000, 0x0000_0000),
      (0x0400_0003, 0x6000_0000, 0x6000_0000), // 29-30 DMA Direction (0=Off, 1=?, 2=CPUtoGP0, 3=GPUREADtoCPU)    ;GP1(04h).0-1
    ];

    for &(val, mask, reg) in values.iter() {
      gpu.gp1(val);
      assert_eq!(gpu.stat() & mask, reg);
    }

    let values = vec![
      (0xE100_000F, 0xF, 0xF), // 0-3   Texture page X Base   (N*64)                              ;GP0(E1h).0-3
      (0xE100_0010, 0x10, 0x10), // 4     Texture page Y Base   (N*256) (ie. 0 or 256)              ;GP0(E1h).4
      (0xE100_0060, 0x60, 0x60), // 5-6   Semi Transparency     (0=B/2+F/2, 1=B+F, 2=B-F, 3=B+F/4)  ;GP0(E1h).5-6
      (0xE100_0180, 0x180, 0x180), // 7-8   Texture page colors   (0=4bit, 1=8bit, 2=15bit, 3=Reserved)GP0(E1h).7-8
      (0xE100_0200, 0x200, 0x200), // 9     Dither 24bit to 15bit (0=Off/strip LSBs, 1=Dither Enabled);GP0(E1h).9
      (0xE100_0400, 0x400, 0x400), // 10    Drawing to display area (0=Prohibited, 1=Allowed)         ;GP0(E1h).10
      (0xE600_0001, 0x800, 0x800), // 11    Set Mask-bit when drawing pixels (0=No, 1=Yes/Mask)       ;GP0(E6h).0
      (0xE600_0002, 0x1000, 0x1000), // 12    Draw Pixels           (0=Always, 1=Not to Masked areas)   ;GP0(E6h).1
      (0xE100_0800, 0x8000, 0x8000), // 15    Texture Disable       (0=Normal, 1=Disable Textures)      ;GP0(E1h).11
      (0x1F00_0000, 0x100_0000, 0x100_0000), // 24    Interrupt Request (IRQ1)    (0=Off, 1=IRQ)       ;GP0(1Fh)/GP1(02h)
    ];

    for &(val, mask, reg) in values.iter() {
      gpu.gp0(val);
      assert_eq!(gpu.stat() & mask, reg);
    }
  }
}
