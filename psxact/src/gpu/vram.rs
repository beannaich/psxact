use super::types::Color;

// VRAM is laid out as a 1024*512 grid
//
// (  0,   0)[16] = 0x0_00_00
// (  1,   0)[16] = 0x0_00_02
// (  0,   1)[16] = 0x0_08_00
// (  1,   1)[16] = 0x0_08_02
// (3FF, 1FF)[16] = 0x7_FF_FF
//
// 1048576 / 512 = 2048 bytes per line.
//
// 16-bit (x, y) = 2048y + 2x
// 24-bit (x, y) = 2048y + 3x

const VRAM_SIZE: usize = 1024 * 1024;
const VRAM_MASK: usize = VRAM_SIZE - 1;

pub struct Vram(Vec<u8>);

impl Vram {
  pub fn new() -> Vram {
    Vram(vec![0; VRAM_SIZE])
  }

  pub fn read_16_data(&self, x: i32, y: i32) -> u16 {
    let addr = addr_16(x, y);
    let buf = &self.0[addr..addr + 2];
    let lsb = buf[0] as u16;
    let msb = buf[1] as u16;
    (msb << 8) | lsb
  }

  pub fn read_16_color(&self, x: i32, y: i32) -> Color {
    Color::from_u16(self.read_16_data(x, y))
  }

  pub fn read_24_color(&self, x: i32, y: i32) -> Color {
    let addr = addr_24(x, y);
    let buf = &self.0[addr..addr + 3];
    let r = buf[0];
    let g = buf[1];
    let b = buf[2];
    Color::from_rgb(r, g, b)
  }

  pub fn write_16_data(&mut self, x: i32, y: i32, val: u16) {
    let addr = addr_16(x, y);
    let buf = &mut self.0[addr..addr + 2];
    buf[0] = (val >> 0) as u8;
    buf[1] = (val >> 8) as u8;
  }
}

#[inline]
fn addr_16(x: i32, y: i32) -> usize {
  let x = x * 2;
  let y = y * 2048;
  ((x + y) as usize) & VRAM_MASK
}

#[inline]
fn addr_24(x: i32, y: i32) -> usize {
  let x = x * 3;
  let y = y * 2048;
  ((x + y) as usize) & VRAM_MASK
}
