pub struct Color {
  r: u8,
  g: u8,
  b: u8,
}

impl Color {
  pub fn from_rgb(r: u8, g: u8, b: u8) -> Color {
    Color { r, g, b }
  }

  pub fn from_u16(val: u16) -> Color {
    let r = (((val as u32) & 0x1F) * 0x21) >> 2;
    let g = (((val as u32) & 0x3E0) * 0x420) >> 12;
    let b = (((val as u32) & 0x7C00) * 0x8400) >> 22;

    Color {
      r: r as u8,
      g: g as u8,
      b: b as u8,
    }
  }

  pub fn from_u24(val: u32) -> Color {
    Color {
      r: (val >> 0) as u8,
      g: (val >> 8) as u8,
      b: (val >> 16) as u8,
    }
  }

  pub fn r(&self) -> u8 {
    self.r
  }

  pub fn g(&self) -> u8 {
    self.g
  }

  pub fn b(&self) -> u8 {
    self.b
  }

  pub fn to_u16(&self) -> u16 {
    let r = (self.r & 0xF8) as u16;
    let g = (self.g & 0xF8) as u16;
    let b = (self.b & 0xF8) as u16;
    (r >> 3) | (g << 2) | (b << 7)
  }

  pub fn to_u32(&self) -> u32 {
    let a = 0xff as u32;
    let r = self.r as u32;
    let g = self.g as u32;
    let b = self.b as u32;
    (a << 24) | (r << 16) | (g << 8) | b
  }

  pub fn blend_mul(&self, color: &Color) -> Color {
    let r = (self.r as u16) * (color.r as u16);
    let g = (self.g as u16) * (color.g as u16);
    let b = (self.b as u16) * (color.b as u16);

    Color {
      r: (r / 0x80).min(0xFF) as u8,
      g: (g / 0x80).min(0xFF) as u8,
      b: (b / 0x80).min(0xFF) as u8,
    }
  }

  pub fn blend_avg(&self, color: &Color) -> Color {
    Color {
      r: (self.r / 2).saturating_add(color.r / 2),
      g: (self.g / 2).saturating_add(color.g / 2),
      b: (self.b / 2).saturating_add(color.b / 2),
    }
  }

  pub fn blend_add(&self, color: &Color) -> Color {
    Color {
      r: color.r.saturating_add(self.r),
      g: color.g.saturating_add(self.g),
      b: color.b.saturating_add(self.b),
    }
  }

  pub fn blend_sub(&self, color: &Color) -> Color {
    Color {
      r: color.r.saturating_sub(self.r),
      g: color.g.saturating_sub(self.g),
      b: color.b.saturating_sub(self.b),
    }
  }

  pub fn blend_add_over_4(&self, color: &Color) -> Color {
    Color {
      r: ((color.r as i32) + ((self.r / 4) as i32)).clamp(0, 255) as u8,
      g: ((color.g as i32) + ((self.g / 4) as i32)).clamp(0, 255) as u8,
      b: ((color.b as i32) + ((self.b / 4) as i32)).clamp(0, 255) as u8,
    }
  }

  pub fn dither(&self, point: &Point) -> Color {
    let x = point.x & 3;
    let y = point.y & 3;
    let dither = DITHER_LUT[y as usize][x as usize];

    Color {
      r: ((self.r as i32) + dither).clamp(0, 255) as u8,
      g: ((self.g as i32) + dither).clamp(0, 255) as u8,
      b: ((self.b as i32) + dither).clamp(0, 255) as u8,
    }
  }
}

const DITHER_LUT: [[i32; 4]; 4] = [[-4, 0, -3, 1], [2, -2, 3, -1], [-3, 1, -4, 0], [3, -1, 2, -2]];

/// Used to represent a color that has come from a texture.
pub struct TextureColor(u16);

impl TextureColor {
  pub fn new(val: u16) -> TextureColor {
    TextureColor(val)
  }

  pub fn to_color(&self) -> Color {
    Color::from_u16(self.0)
  }

  pub fn is_semi_transparent(&self) -> bool {
    (self.0 & 0x8000) != 0
  }

  pub fn is_full_transparent(&self) -> bool {
    self.0 == 0
  }
}

pub struct Point {
  pub x: i32,
  pub y: i32,
}

impl Point {
  pub fn from_u24(val: u32, x_offset: i32, y_offset: i32) -> Point {
    Point {
      x: x_offset + Self::snip(val),
      y: y_offset + Self::snip(val >> 16),
    }
  }

  fn snip(val: u32) -> i32 {
    (((val & 0x7FF) ^ 0x400) - 0x400) as i32
  }
}

pub struct TextureCoord {
  pub u: u8,
  pub v: u8,
}

impl TextureCoord {
  pub fn from_u16(val: u16) -> TextureCoord {
    TextureCoord {
      u: (val >> 0) as u8,
      v: (val >> 8) as u8,
    }
  }
}

pub struct TextureParams {
  // from 'palette'
  pub palette_page_x: i32,
  pub palette_page_y: i32,

  // from 'texpage'
  pub texture_colors: i32,
  pub texture_page_x: i32,
  pub texture_page_y: i32,
  pub color_mix_mode: i32,
}

pub struct Triangle<'a> {
  colors: &'a [Color],
  coords: &'a [TextureCoord],
  points: &'a [Point],
  params: &'a TextureParams,
  indices: [usize; 3],
}

impl<'a> Triangle<'a> {
  pub fn new(
    params: &'a TextureParams,
    points: &'a [Point],
    colors: &'a [Color],
    coords: &'a [TextureCoord],
  ) -> Triangle<'a> {
    Triangle {
      params,
      colors,
      coords,
      points,
      indices: if Self::is_clockwise(&points) {
        [0, 1, 2]
      } else {
        [0, 2, 1]
      },
    }
  }

  fn is_clockwise(p: &[Point]) -> bool {
    let sum = (p[1].x - p[0].x) * (p[1].y + p[0].y)
      + (p[2].x - p[1].x) * (p[2].y + p[1].y)
      + (p[0].x - p[2].x) * (p[0].y + p[2].y);

    sum >= 0
  }

  pub fn color(&self, n: usize) -> &Color {
    &self.colors[self.indices[n]]
  }

  pub fn coord(&self, n: usize) -> &TextureCoord {
    &self.coords[self.indices[n]]
  }

  pub fn point(&self, n: usize) -> &Point {
    &self.points[self.indices[n]]
  }

  pub fn params(&self) -> &TextureParams {
    self.params
  }

  pub fn coord_lerp(&self, w0: i32, w1: i32, w2: i32) -> TextureCoord {
    let t0 = self.coord(0);
    let t1 = self.coord(1);
    let t2 = self.coord(2);
    TextureCoord {
      u: lerp(t0.u, t1.u, t2.u, w0, w1, w2) as u8,
      v: lerp(t0.v, t1.v, t2.v, w0, w1, w2) as u8,
    }
  }

  pub fn color_lerp(&self, w0: i32, w1: i32, w2: i32) -> Color {
    let c0 = self.color(0);
    let c1 = self.color(1);
    let c2 = self.color(2);
    let r = lerp(c0.r(), c1.r(), c2.r(), w0, w1, w2) as u8;
    let g = lerp(c0.g(), c1.g(), c2.g(), w0, w1, w2) as u8;
    let b = lerp(c0.b(), c1.b(), c2.b(), w0, w1, w2) as u8;
    Color::from_rgb(r, g, b)
  }
}

fn lerp<T: Into<i32>>(a: T, b: T, c: T, w0: i32, w1: i32, w2: i32) -> i32 {
  let a = a.into();
  let b = b.into();
  let c = c.into();
  ((w0 * a) + (w1 * b) + (w2 * c)) / (w0 + w1 + w2)
}

pub struct Gp0Command(u32);

impl Gp0Command {
  /*
    27-28 Rect Size   (0=Var, 1=1x1, 2=8x8, 3=16x16) (Rectangle only)
    29-31 Primitive Type    (1=Polygon, 2=Line, 3=Rectangle)
  */

  #[inline]
  pub fn new(val: u32) -> Gp0Command {
    Gp0Command(val)
  }

  #[inline]
  pub fn is_raw_texture(&self) -> bool {
    (self.0 & (1 << 24)) != 0
  }

  #[inline]
  pub fn is_semi_transparent(&self) -> bool {
    (self.0 & (1 << 25)) != 0
  }

  #[inline]
  pub fn is_texture_mapped(&self) -> bool {
    (self.0 & (1 << 26)) != 0
  }

  #[inline]
  pub fn is_poly_line(&self) -> bool {
    (self.0 & (1 << 27)) != 0
  }

  #[inline]
  pub fn is_quad_poly(&self) -> bool {
    (self.0 & (1 << 27)) != 0
  }

  #[inline]
  pub fn is_gouraud_shaded(&self) -> bool {
    (self.0 & (1 << 28)) != 0
  }
}
