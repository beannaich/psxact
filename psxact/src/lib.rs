pub mod addr;
pub mod cdrom;
pub mod console;
pub mod cpu;
pub mod exp;
pub mod gpu;
pub mod input;
pub mod mdec;
pub mod memory;
pub mod spu;
pub mod timer;
pub mod timing;
pub mod util;

#[derive(Clone, Copy)]
pub enum InterruptType {
  VBlank = 0x1,
  Gpu = 0x2,
  Cdrom = 0x4,
  Dma = 0x8,
  Timer0 = 0x10,
  Timer1 = 0x20,
  Timer2 = 0x40,
  Input = 0x80,
  Sio = 0x100,
  Spu = 0x200,
  Pio = 0x400,
}

#[derive(PartialEq)]
pub enum AddressWidth {
  Byte = 1,
  Half = 2,
  Word = 4,
}

pub enum BiosPatch {
  DebugTty,
  SkipIntro,
}

/// Encapsulates the behavior necessary for a component to be mapped into the CPU address space.
pub trait IO {
  /// Reads a datum from a component.
  ///
  /// ## Parameters
  ///
  /// * `width` - The size of the returned datum (8, 16, or 32-bit).
  /// * `address` - The component specific location to process.
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32;

  /// Writes a datum to a component.
  ///
  /// ## Parameters
  ///
  /// * `width` - The size of the `val` parameter (8, 16, or 32-bit).
  /// * `address` - The component specific location to process the `val` parameter.
  /// * `val` - The datum to write to the component.
  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32);
}

/// Encapsulates the behavior necessary for a component to be the target of a DMA.
pub trait Comms {
  /// Returns the number of cycles needed for a single DMA access.
  fn dma_speed(&mut self) -> i32;

  /// Returns true if the DMA target can be read from, false otherwise.
  fn dma_read_ready(&mut self) -> bool;

  /// Returns true if the DMA target can be written to, false otherwise.
  fn dma_write_ready(&mut self) -> bool;

  /// Reads a single `word` from the DMA target.
  fn dma_read(&mut self) -> u32;

  /// Writes a single `word` to the DMA target.
  fn dma_write(&mut self, val: u32);
}
