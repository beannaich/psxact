pub struct CpuRegisterFile([u32; 32]);

impl CpuRegisterFile {
  pub fn new() -> CpuRegisterFile {
    CpuRegisterFile([0; 32])
  }

  #[inline]
  pub fn get(&self, reg: u32) -> u32 {
    self.0[reg as usize]
  }

  #[inline]
  pub fn set(&mut self, reg: u32, val: u32) {
    if reg != 0 {
      self.0[reg as usize] = val;
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_get_works() {
    let reg = CpuRegisterFile::new();

    let read1 = reg.get(4);
    let read2 = reg.get(4);

    assert_eq!(read1, read2);
  }

  #[test]
  fn test_set_works() {
    let mut reg = CpuRegisterFile::new();

    reg.set(4, 0xCAFE_BABE);
    assert_eq!(reg.get(4), 0xCAFE_BABE);

    reg.set(4, 0);
    assert_eq!(reg.get(4), 0);
  }

  #[test]
  fn test_reg_0_is_always_0() {
    let mut reg = CpuRegisterFile::new();

    reg.set(0, 0xFFFF_FFFF);
    assert_eq!(reg.get(0), 0);
  }
}
