use super::cop::CoProcessor;

#[derive(Clone, Copy, PartialEq)]
pub enum Matrix {
  Rot = 0,
  Llm = 1,
  Lcm = 2,
  Nil = 3,
}

#[derive(Clone, Copy, PartialEq)]
pub enum Vector {
  Tr = 0,
  Bk = 1,
  Fc = 2,
  Zr = 3,
}

#[derive(Clone, Copy, Default)]
pub struct GteColor {
  r: u8,
  g: u8,
  b: u8,
  c: u8,
}

impl GteColor {
  pub fn new(val: u32) -> GteColor {
    GteColor {
      r: (val >> (8 * 0)) as u8,
      g: (val >> (8 * 1)) as u8,
      b: (val >> (8 * 2)) as u8,
      c: (val >> (8 * 3)) as u8,
    }
  }

  pub fn value(&self) -> u32 {
    let r = self.r as u32;
    let g = self.g as u32;
    let b = self.b as u32;
    let c = self.c as u32;
    (c << 24) | (b << 16) | (g << 8) | r
  }
}

pub struct GteCcr {
  matrix: [[[i32; 3]; 3]; 4],
  vector: [[i32; 3]; 4],
  ofx: i32,
  ofy: i32,
  h: i32,
  dqa: i32,
  dqb: i32,
  zsf3: i32,
  zsf4: i32,
  flag: u32,
}

impl GteCcr {
  pub fn new() -> Self {
    Self {
      matrix: [[[0; 3]; 3]; 4],
      vector: [[0; 3]; 4],
      ofx: 0,
      ofy: 0,
      h: 0,
      dqa: 0,
      dqb: 0,
      zsf3: 0,
      zsf4: 0,
      flag: 0,
    }
  }
}

pub struct GteGpr {
  vector: [[i32; 3]; 4],
  rgbc: GteColor,
  otz: i32,
  ir0: i32,
  sx: [i32; 3],
  sy: [i32; 3],
  sz: [i32; 4],
  rgb: [GteColor; 3],
  res: i32,
  mac: [i32; 4],
  lzcs: i32,
  lzcr: i32,
}

impl GteGpr {
  pub fn new() -> GteGpr {
    GteGpr {
      vector: [[0; 3]; 4],
      rgbc: GteColor::default(),
      otz: 0,
      ir0: 0,
      sx: [0; 3],
      sy: [0; 3],
      sz: [0; 4],
      rgb: [GteColor::default(); 3],
      res: 0,
      mac: [0; 4],
      lzcs: 0,
      lzcr: 0,
    }
  }
}

pub struct GteCore {
  ccr: GteCcr,
  gpr: GteGpr,
}

impl GteCore {
  pub fn new() -> GteCore {
    GteCore {
      ccr: GteCcr::new(),
      gpr: GteGpr::new(),
    }
  }

  pub fn read_gpr(&mut self, n: u32) -> u32 {
    match n {
      0x00 => (self.gpr.vector[0][0] as u16 as u32) | ((self.gpr.vector[0][1] as u16 as u32) << 16),
      0x01 => (self.gpr.vector[0][2]) as u32,
      0x02 => (self.gpr.vector[1][0] as u16 as u32) | ((self.gpr.vector[1][1] as u16 as u32) << 16),
      0x03 => (self.gpr.vector[1][2]) as u32,
      0x04 => (self.gpr.vector[2][0] as u16 as u32) | ((self.gpr.vector[2][1] as u16 as u32) << 16),
      0x05 => (self.gpr.vector[2][2]) as u32,
      0x06 => self.gpr.rgbc.value(),
      0x07 => (self.gpr.otz) as u32,
      0x08 => (self.gpr.ir0) as u32,
      0x09 => (self.gpr.vector[3][0]) as u32,
      0x0A => (self.gpr.vector[3][1]) as u32,
      0x0B => (self.gpr.vector[3][2]) as u32,
      0x0C => (self.gpr.sx[0] as u16 as u32) | ((self.gpr.sy[0] as u16 as u32) << 16),
      0x0D => (self.gpr.sx[1] as u16 as u32) | ((self.gpr.sy[1] as u16 as u32) << 16),
      0x0E | 0x0F => (self.gpr.sx[2] as u16 as u32) | ((self.gpr.sy[2] as u16 as u32) << 16),
      0x10 => (self.gpr.sz[0]) as u32,
      0x11 => (self.gpr.sz[1]) as u32,
      0x12 => (self.gpr.sz[2]) as u32,
      0x13 => (self.gpr.sz[3]) as u32,
      0x14 => self.gpr.rgb[0].value(),
      0x15 => self.gpr.rgb[1].value(),
      0x16 => self.gpr.rgb[2].value(),
      0x17 => (self.gpr.res) as u32,
      0x18 => (self.gpr.mac[0]) as u32,
      0x19 => (self.gpr.mac[1]) as u32,
      0x1A => (self.gpr.mac[2]) as u32,
      0x1B => (self.gpr.mac[3]) as u32,
      0x1C | 0x1D => {
        (((self.gpr.vector[3][0] >> 7).clamp(0, 31) << (5 * 0))
          | ((self.gpr.vector[3][1] >> 7).clamp(0, 31) << (5 * 1))
          | ((self.gpr.vector[3][2] >> 7).clamp(0, 31) << (5 * 2))) as u32
      }

      0x1E => return (self.gpr.lzcs) as u32,
      0x1F => return (self.gpr.lzcr) as u32,
      _ => return 0,
    }
  }

  pub fn write_gpr(&mut self, n: u32, value: u32) {
    match n {
      0x00 => {
        self.gpr.vector[0][0] = value as i16 as i32;
        self.gpr.vector[0][1] = (value >> 16) as i16 as i32;
      }
      0x01 => self.gpr.vector[0][2] = value as i16 as i32,
      0x02 => {
        self.gpr.vector[1][0] = value as i16 as i32;
        self.gpr.vector[1][1] = (value >> 16) as i16 as i32;
      }
      0x03 => self.gpr.vector[1][2] = value as i16 as i32,
      0x04 => {
        self.gpr.vector[2][0] = value as i16 as i32;
        self.gpr.vector[2][1] = (value >> 16) as i16 as i32;
      }
      0x05 => self.gpr.vector[2][2] = value as i16 as i32,
      0x06 => self.gpr.rgbc = GteColor::new(value),
      0x07 => self.gpr.otz = value as u16 as i32,
      0x08 => self.gpr.ir0 = value as i16 as i32,
      0x09 => self.gpr.vector[3][0] = value as i16 as i32,
      0x0A => self.gpr.vector[3][1] = value as i16 as i32,
      0x0B => self.gpr.vector[3][2] = value as i16 as i32,
      0x0C => {
        self.gpr.sx[0] = value as i16 as i32;
        self.gpr.sy[0] = (value >> 16) as i16 as i32;
      }
      0x0D => {
        self.gpr.sx[1] = value as i16 as i32;
        self.gpr.sy[1] = (value >> 16) as i16 as i32;
      }
      0x0E => {
        self.gpr.sx[2] = value as i16 as i32;
        self.gpr.sy[2] = (value >> 16) as i16 as i32;
      }
      0x0F => {
        self.gpr.sx[0] = self.gpr.sx[1];
        self.gpr.sx[1] = self.gpr.sx[2];
        self.gpr.sx[2] = value as i16 as i32;

        self.gpr.sy[0] = self.gpr.sy[1];
        self.gpr.sy[1] = self.gpr.sy[2];
        self.gpr.sy[2] = (value >> 16) as i16 as i32;
      }
      0x10 => self.gpr.sz[0] = value as u16 as i32,
      0x11 => self.gpr.sz[1] = value as u16 as i32,
      0x12 => self.gpr.sz[2] = value as u16 as i32,
      0x13 => self.gpr.sz[3] = value as u16 as i32,
      0x14 => self.gpr.rgb[0] = GteColor::new(value),
      0x15 => self.gpr.rgb[1] = GteColor::new(value),
      0x16 => self.gpr.rgb[2] = GteColor::new(value),
      0x17 => self.gpr.res = value as i32,
      0x18 => self.gpr.mac[0] = value as i32,
      0x19 => self.gpr.mac[1] = value as i32,
      0x1A => self.gpr.mac[2] = value as i32,
      0x1B => self.gpr.mac[3] = value as i32,
      0x1C => {
        self.gpr.vector[3][0] = ((value & 0x001F) << 7) as i32;
        self.gpr.vector[3][1] = ((value & 0x03E0) << 2) as i32;
        self.gpr.vector[3][2] = ((value & 0x7C00) >> 3) as i32;
      }
      0x1D => {}
      0x1E => {
        self.gpr.lzcs = value as i32;
        self.gpr.lzcr = if self.gpr.lzcs < 0 {
          (!value).leading_zeros() as i32
        } else {
          value.leading_zeros() as i32
        };
      }
      0x1F => {}
      _ => panic!(""),
    }
  }

  pub fn read_matrix_vector_group(&mut self, n: u32) -> u32 {
    let matrix = &self.ccr.matrix[(n as usize) >> 3];
    let vector = &self.ccr.vector[(n as usize) >> 3];

    match n & 7 {
      0 => (matrix[0][0] as u16 as u32) | ((matrix[0][1] as u16 as u32) << 16),
      1 => (matrix[0][2] as u16 as u32) | ((matrix[1][0] as u16 as u32) << 16),
      2 => (matrix[1][1] as u16 as u32) | ((matrix[1][2] as u16 as u32) << 16),
      3 => (matrix[2][0] as u16 as u32) | ((matrix[2][1] as u16 as u32) << 16),
      4 => matrix[2][2] as u32,
      5 => vector[0] as u32,
      6 => vector[1] as u32,
      _ => vector[2] as u32,
    }
  }

  pub fn read_ccr(&mut self, n: u32) -> u32 {
    if n <= 0x17 {
      return self.read_matrix_vector_group(n);
    }

    match n {
      0x18 => self.ccr.ofx as u32,
      0x19 => self.ccr.ofy as u32,
      0x1A => self.ccr.h as i16 as u32,
      0x1B => self.ccr.dqa as u32,
      0x1C => self.ccr.dqb as u32,
      0x1D => self.ccr.zsf3 as u32,
      0x1E => self.ccr.zsf4 as u32,
      0x1F => self.ccr.flag,
      _ => 0,
    }
  }

  pub fn write_matrix_vector_group(&mut self, n: u32, value: u32) {
    let matrix = &mut self.ccr.matrix[(n as usize) >> 3];
    let vector = &mut self.ccr.vector[(n as usize) >> 3];

    match n & 7 {
      0 => {
        matrix[0][0] = (value) as i16 as i32;
        matrix[0][1] = (value >> 16) as i16 as i32;
      }
      1 => {
        matrix[0][2] = (value) as i16 as i32;
        matrix[1][0] = (value >> 16) as i16 as i32;
      }
      2 => {
        matrix[1][1] = (value) as i16 as i32;
        matrix[1][2] = (value >> 16) as i16 as i32;
      }
      3 => {
        matrix[2][0] = (value) as i16 as i32;
        matrix[2][1] = (value >> 16) as i16 as i32;
      }
      4 => matrix[2][2] = (value) as i16 as i32,
      5 => vector[0] = value as i32,
      6 => vector[1] = value as i32,
      _ => vector[2] = value as i32,
    }
  }

  pub fn write_ccr(&mut self, n: u32, value: u32) {
    if n <= 0x17 {
      return self.write_matrix_vector_group(n, value);
    }

    match n {
      0x18 => self.ccr.ofx = value as i32,
      0x19 => self.ccr.ofy = value as i32,
      0x1A => self.ccr.h = value as u16 as i32,
      0x1B => self.ccr.dqa = value as i16 as i32,
      0x1C => self.ccr.dqb = value as i32,
      0x1D => self.ccr.zsf3 = value as i16 as i32,
      0x1E => self.ccr.zsf4 = value as i16 as i32,
      0x1F => {
        let msb = ((value & 0x7F87_E000) != 0) as u32;
        self.ccr.flag = (value & 0x7FFF_F000) | (msb << 31);
      }
      _ => {
        panic!("")
      }
    }
  }

  pub fn set_flag(&mut self, flag: i32) {
    self.ccr.flag |= 1 << flag;
  }

  pub fn flag_a(&mut self, n: i32, value: i64) -> i64 {
    const MAX: i64 = (1 << (44 - 1)) - 1;
    const MIN: i64 = 0 - (1 << (44 - 1));

    if value > MAX {
      self.set_flag(A1_MAX - n);
    } else if value < MIN {
      self.set_flag(A1_MIN - n);
    }

    return (value << 20) >> 20;
  }

  pub fn flag_b(&mut self, n: i32, code: u32, value: i32) -> i32 {
    let max = 0x7FFF;
    let min = get_lm(code);

    if value < min {
      self.set_flag(B1 - n);
      return min;
    }

    if value > max {
      self.set_flag(B1 - n);
      return max;
    }

    return value;
  }

  pub fn flag_b_shifted(&mut self, n: i32, code: u32, value: i32, shifted: i32) -> i32 {
    const MAX: i32 = 32767;
    const MIN: i32 = -32768;

    if shifted < MIN || shifted > MAX {
      self.set_flag(B1 - n);
    }

    let tmp = get_lm(code);

    if value < tmp {
      return tmp;
    }

    if value > MAX {
      return MAX;
    }

    return value;
  }

  pub fn flag_c(&mut self, n: i32, value: i32) -> i32 {
    const MAX: i32 = 255;
    const MIN: i32 = 0;

    if value < MIN {
      self.set_flag(C1 - n);
      return MIN;
    }

    if value > MAX {
      self.set_flag(C1 - n);
      return MAX;
    }

    return value;
  }

  pub fn flag_d(&mut self, value: i32) -> i32 {
    const MAX: i32 = 0xFFFF;
    const MIN: i32 = 0;

    if value < MIN {
      self.set_flag(D);
      return MIN;
    }

    if value > MAX {
      self.set_flag(D);
      return MAX;
    }

    return value;
  }

  pub fn flag_e(&mut self) -> u32 {
    self.set_flag(E);
    return 0x1_FFFF;
  }

  pub fn flag_f(&mut self, value: i64) -> i64 {
    const MAX: i64 = 0x7FFF_FFFF;
    const MIN: i64 = -0x8000_0000;

    if value < MIN {
      self.set_flag(F_MIN);
    }

    if value > MAX {
      self.set_flag(F_MAX);
    }

    return value;
  }

  pub fn flag_g(&mut self, n: i32, value: i32) -> i32 {
    const MAX: i32 = 0x3FF;
    const MIN: i32 = -0x400;

    if value < MIN {
      self.set_flag(G1 - n);
      return MIN;
    }

    if value > MAX {
      self.set_flag(G1 - n);
      return MAX;
    }

    return value;
  }

  pub fn flag_h(&mut self, value: i64) -> i32 {
    const MAX: i64 = 0x1000; // TODO: why is this one different?
    const MIN: i64 = 0;

    if value < MIN {
      self.set_flag(H);
      return MIN as i32;
    }

    if value > MAX {
      self.set_flag(H);
      return MAX as i32;
    }

    value as i32
  }

  pub fn depth_cue(&mut self, code: u32, r: i32, g: i32, b: i32) {
    let rfc = (self.ccr.vector[Vector::Fc as usize][0] as i64) << 12;
    let gfc = (self.ccr.vector[Vector::Fc as usize][1] as i64) << 12;
    let bfc = (self.ccr.vector[Vector::Fc as usize][2] as i64) << 12;

    let shift = get_sf(code);

    let ir0 = self.gpr.ir0 as i64;
    let (r, g, b) = (r as i64, g as i64, b as i64);

    self.gpr.mac[1] = (self.flag_a(0, rfc - r) >> shift) as i32;
    self.gpr.mac[2] = (self.flag_a(1, gfc - g) >> shift) as i32;
    self.gpr.mac[3] = (self.flag_a(2, bfc - b) >> shift) as i32;

    let mac1 = self.flag_b(0, 0, self.gpr.mac[1]) as i64;
    let mac2 = self.flag_b(1, 0, self.gpr.mac[2]) as i64;
    let mac3 = self.flag_b(2, 0, self.gpr.mac[3]) as i64;

    self.gpr.mac[1] = (self.flag_a(0, r + ir0 * mac1) >> shift) as i32;
    self.gpr.mac[2] = (self.flag_a(1, g + ir0 * mac2) >> shift) as i32;
    self.gpr.mac[3] = (self.flag_a(2, b + ir0 * mac3) >> shift) as i32;

    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn divide(&mut self) -> u32 {
    if self.gpr.sz[3] <= (self.ccr.h / 2) {
      return self.flag_e();
    }

    let z: u32 = (self.gpr.sz[3] as u16).leading_zeros();
    let n: u64 = (self.ccr.h as u64) << z;
    let d: u32 = (self.gpr.sz[3] as u32) << z;
    let u: u32 = (UNR_TABLE[(((d & 0x7FFF) + 0x40) >> 7) as usize] as u32) + 0x101;

    let d = ((0x200_0080 - (d * u)) >> (8 * 1)) as u32;
    let d = ((0x000_0080 + (d * u)) >> (8 * 1)) as u64;
    let d = ((0x000_8000 + (d * n)) >> (8 * 2)) as u32;
    d.min(0x1_FFFF)
  }

  pub fn mac_to_ir(&mut self, code: u32) {
    self.gpr.vector[3][0] = self.flag_b(0, code, self.gpr.mac[1]);
    self.gpr.vector[3][1] = self.flag_b(1, code, self.gpr.mac[2]);
    self.gpr.vector[3][2] = self.flag_b(2, code, self.gpr.mac[3]);
  }

  pub fn mac_to_rgb(&mut self) {
    self.gpr.rgb[0] = self.gpr.rgb[1];
    self.gpr.rgb[1] = self.gpr.rgb[2];

    self.gpr.rgb[2].r = self.flag_c(0, self.gpr.mac[1] >> 4) as u8;
    self.gpr.rgb[2].g = self.flag_c(1, self.gpr.mac[2] >> 4) as u8;
    self.gpr.rgb[2].b = self.flag_c(2, self.gpr.mac[3] >> 4) as u8;
    self.gpr.rgb[2].c = self.gpr.rgbc.c;
  }

  pub fn transform(&mut self, code: u32, mx: Matrix, cv: Vector, v: i32) -> i64 {
    let mut mac = 0;

    let shift = get_sf(code);

    for i in 0..3 {
      let matrix = &self.ccr.matrix[mx as usize];
      let vector = &self.gpr.vector[v as usize];
      let x = (matrix[i][0] as i64) * (vector[0] as i64);
      let y = (matrix[i][1] as i64) * (vector[1] as i64);
      let z = (matrix[i][2] as i64) * (vector[2] as i64);

      let offset = &self.ccr.vector[cv as usize];
      mac = (offset[i] as i64) << 12;
      mac = self.flag_a(i as i32, mac + x);
      mac = self.flag_a(i as i32, mac + y);
      mac = self.flag_a(i as i32, mac + z);

      self.gpr.mac[1 + i] = (mac >> shift) as i32;
    }

    return mac;
  }

  pub fn transform_buggy(&mut self, code: u32, mx: Matrix, cv: Vector, v: i32) {
    let shift = get_sf(code);

    for i in 0..3 {
      let mulr;

      let offset = &self.ccr.vector[cv as usize];
      let mut mac = (offset[i] as i64) << 12;

      let vector = &self.gpr.vector[v as usize];

      if mx == Matrix::Nil {
        if i == 0 {
          let a = (self.gpr.rgbc.r as i32) << 4;
          let b = self.gpr.ir0;
          mulr = [a * vector[0] * -1, a * vector[1], b * vector[2]]
        } else {
          let cr = if i == 1 {
            self.ccr.matrix[Matrix::Rot as usize][0][2]
          } else {
            self.ccr.matrix[Matrix::Rot as usize][1][1]
          };

          mulr = [cr * vector[0], cr * vector[1], cr * vector[2]]
        }
      } else {
        let matrix = &self.ccr.matrix[mx as usize];
        mulr = [matrix[i][0] * vector[0], matrix[i][1] * vector[1], matrix[i][2] * vector[2]]
      }

      mac = self.flag_a(i as i32, mac + (mulr[0] as i64));

      if cv == Vector::Fc {
        self.flag_b(i as i32, 0, (mac >> shift) as i32);
        mac = 0;
      }

      mac = self.flag_a(i as i32, mac + (mulr[1] as i64));
      mac = self.flag_a(i as i32, mac + (mulr[2] as i64));

      self.gpr.mac[1 + i] = (mac >> shift) as i32;
    }
  }

  pub fn transform_dq(&mut self, div: i64) {
    let a = self.ccr.dqa as i64;
    let b = self.ccr.dqb as i64;
    let c = (a * div) + b;
    self.gpr.mac[0] = self.flag_f(c) as i32;
    self.gpr.ir0 = self.flag_h(c >> 12);
  }

  pub fn transform_xy(&mut self, div: i64) {
    self.gpr.mac[0] = (self.flag_f((self.ccr.ofx as i64) + (self.gpr.vector[3][0] as i64) * div) >> 16) as i32;

    self.gpr.sx[0] = self.gpr.sx[1];
    self.gpr.sx[1] = self.gpr.sx[2];
    self.gpr.sx[2] = self.flag_g(0, self.gpr.mac[0]);

    self.gpr.mac[0] = (self.flag_f((self.ccr.ofy as i64) + (self.gpr.vector[3][1] as i64) * div) >> 16) as i32;

    self.gpr.sy[0] = self.gpr.sy[1];
    self.gpr.sy[1] = self.gpr.sy[2];
    self.gpr.sy[2] = self.flag_g(1, self.gpr.mac[0]);
  }

  pub fn transform_pt(&mut self, code: u32, mx: Matrix, cv: Vector, v: i32) -> i64 {
    let z = (self.transform(code, mx, cv, v) >> 12) as i32;

    self.gpr.vector[3][0] = self.flag_b(0, code, self.gpr.mac[1]);
    self.gpr.vector[3][1] = self.flag_b(1, code, self.gpr.mac[2]);
    self.gpr.vector[3][2] = self.flag_b_shifted(2, code, self.gpr.mac[3], z);

    self.gpr.sz[0] = self.gpr.sz[1];
    self.gpr.sz[1] = self.gpr.sz[2];
    self.gpr.sz[2] = self.gpr.sz[3];
    self.gpr.sz[3] = self.flag_d(z);
    self.divide() as i64
  }

  pub fn run(&mut self, code: u32) {
    self.ccr.flag = 0;

    match code & 0x3F {
      0x00 => self.op_rtps(code),
      0x01 => self.op_rtps(code),
      0x06 => self.op_nclip(),
      0x0C => self.op_op(code),
      0x10 => self.op_dpcs(code),
      0x11 => self.op_intpl(code),
      0x12 => self.op_mvmva(code),
      0x13 => self.op_ncds(code),
      0x14 => self.op_cdp(code),
      0x16 => self.op_ncdt(code),
      0x1A => self.op_dcpl(code),
      0x1B => self.op_nccs(code),
      0x1C => self.op_cc(code),
      0x1E => self.op_ncs(code),
      0x20 => self.op_nct(code),
      0x28 => self.op_sqr(code),
      0x29 => self.op_dcpl(code),
      0x2A => self.op_dpct(code),
      0x2D => self.op_avsz3(),
      0x2E => self.op_avsz4(),
      0x30 => self.op_rtpt(code),
      0x3D => self.op_gpf(code),
      0x3E => self.op_gpl(code),
      0x3F => self.op_ncct(code),

      _ => {
        panic!("cop2::run(0x{:08x})", code);
      }
    }

    let msb = ((self.ccr.flag & 0x7F87_E000) != 0) as u32;
    self.ccr.flag = self.ccr.flag | (msb << 31);
  }

  pub fn op_avsz3(&mut self) {
    let temp = (self.ccr.zsf3 as i64) * ((self.gpr.sz[1] + self.gpr.sz[2] + self.gpr.sz[3]) as i64);

    self.gpr.mac[0] = self.flag_f(temp) as i32;
    self.gpr.otz = self.flag_d((temp >> 12) as i32);
  }

  pub fn op_avsz4(&mut self) {
    let temp = (self.ccr.zsf4 as i64) * ((self.gpr.sz[0] + self.gpr.sz[1] + self.gpr.sz[2] + self.gpr.sz[3]) as i64);

    self.gpr.mac[0] = self.flag_f(temp) as i32;
    self.gpr.otz = self.flag_d((temp >> 12) as i32);
  }

  pub fn op_cc(&mut self, code: u32) {
    self.transform(code, Matrix::Lcm, Vector::Bk, 3);
    self.mac_to_ir(code);

    let shift = get_sf(code);

    let r = (self.gpr.rgbc.r as i64) << 4;
    let g = (self.gpr.rgbc.g as i64) << 4;
    let b = (self.gpr.rgbc.b as i64) << 4;

    self.gpr.mac[1] = (self.flag_a(0, r * (self.gpr.vector[3][0] as i64)) >> shift) as i32;
    self.gpr.mac[2] = (self.flag_a(1, g * (self.gpr.vector[3][1] as i64)) >> shift) as i32;
    self.gpr.mac[3] = (self.flag_a(2, b * (self.gpr.vector[3][2] as i64)) >> shift) as i32;

    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn op_cdp(&mut self, code: u32) {
    self.transform(code, Matrix::Lcm, Vector::Bk, 3);
    self.mac_to_ir(code);

    let r = ((self.gpr.rgbc.r as i32) << 4) * self.gpr.vector[3][0];
    let g = ((self.gpr.rgbc.g as i32) << 4) * self.gpr.vector[3][1];
    let b = ((self.gpr.rgbc.b as i32) << 4) * self.gpr.vector[3][2];

    self.depth_cue(code, r, g, b);
  }

  pub fn op_dcpl(&mut self, code: u32) {
    let r = ((self.gpr.rgbc.r as i32) << 4) * self.gpr.vector[3][0];
    let g = ((self.gpr.rgbc.g as i32) << 4) * self.gpr.vector[3][1];
    let b = ((self.gpr.rgbc.b as i32) << 4) * self.gpr.vector[3][2];

    self.depth_cue(code, r, g, b);
  }

  pub fn op_dpcs(&mut self, code: u32) {
    let r = (self.gpr.rgbc.r as i32) << 16;
    let g = (self.gpr.rgbc.g as i32) << 16;
    let b = (self.gpr.rgbc.b as i32) << 16;

    self.depth_cue(code, r, g, b);
  }

  pub fn op_dpct(&mut self, code: u32) {
    for _ in 0..3 {
      let r = (self.gpr.rgb[0].r as i32) << 16;
      let g = (self.gpr.rgb[0].g as i32) << 16;
      let b = (self.gpr.rgb[0].b as i32) << 16;

      self.depth_cue(code, r, g, b);
    }
  }

  pub fn op_gpf(&mut self, code: u32) {
    let shift = get_sf(code);

    self.gpr.mac[1] = (self.gpr.ir0 * self.gpr.vector[3][0]) >> shift;
    self.gpr.mac[2] = (self.gpr.ir0 * self.gpr.vector[3][1]) >> shift;
    self.gpr.mac[3] = (self.gpr.ir0 * self.gpr.vector[3][2]) >> shift;

    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn op_gpl(&mut self, code: u32) {
    let shift = get_sf(code);

    let mac1 = (self.gpr.mac[1] as i64) << shift;
    let mac2 = (self.gpr.mac[2] as i64) << shift;
    let mac3 = (self.gpr.mac[3] as i64) << shift;

    self.gpr.mac[1] = (self.flag_a(0, mac1 + ((self.gpr.ir0 as i64) * (self.gpr.vector[3][0] as i64))) >> shift) as i32;
    self.gpr.mac[2] = (self.flag_a(1, mac2 + ((self.gpr.ir0 as i64) * (self.gpr.vector[3][1] as i64))) >> shift) as i32;
    self.gpr.mac[3] = (self.flag_a(2, mac3 + ((self.gpr.ir0 as i64) * (self.gpr.vector[3][2] as i64))) >> shift) as i32;

    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn op_intpl(&mut self, code: u32) {
    let fc = &self.ccr.vector[Vector::Fc as usize];

    let rfc = (fc[0] as i64) << 12;
    let gfc = (fc[1] as i64) << 12;
    let bfc = (fc[2] as i64) << 12;

    let shift = get_sf(code);

    self.gpr.mac[1] = (self.flag_a(0, rfc - ((self.gpr.vector[3][0] as i64) << 12)) >> shift) as i32;
    self.gpr.mac[2] = (self.flag_a(1, gfc - ((self.gpr.vector[3][1] as i64) << 12)) >> shift) as i32;
    self.gpr.mac[3] = (self.flag_a(2, bfc - ((self.gpr.vector[3][2] as i64) << 12)) >> shift) as i32;

    let mac1 = (self.gpr.ir0 as i64) * (self.flag_b(0, 0, self.gpr.mac[1]) as i64);
    let mac2 = (self.gpr.ir0 as i64) * (self.flag_b(1, 0, self.gpr.mac[2]) as i64);
    let mac3 = (self.gpr.ir0 as i64) * (self.flag_b(2, 0, self.gpr.mac[3]) as i64);

    self.gpr.mac[1] = (self.flag_a(0, (((self.gpr.vector[3][0] as i64) << 12) + mac1) >> shift)) as i32;
    self.gpr.mac[2] = (self.flag_a(1, (((self.gpr.vector[3][1] as i64) << 12) + mac2) >> shift)) as i32;
    self.gpr.mac[3] = (self.flag_a(2, (((self.gpr.vector[3][2] as i64) << 12) + mac3) >> shift)) as i32;

    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn op_mvmva(&mut self, code: u32) {
    let mx = get_mx(code);
    let cv = get_cv(code);
    let v = get_v(code);

    self.transform_buggy(code, mx, cv, v);

    self.mac_to_ir(code);
  }

  pub fn op_nccs(&mut self, code: u32) {
    self.transform(code, Matrix::Llm, Vector::Zr, 0);
    self.mac_to_ir(code);

    self.op_cc(code);
  }

  pub fn op_ncct(&mut self, code: u32) {
    for i in 0..3 {
      self.transform(code, Matrix::Llm, Vector::Zr, i);
      self.mac_to_ir(code);

      self.op_cc(code);
    }
  }

  pub fn op_ncds(&mut self, code: u32) {
    self.transform(code, Matrix::Llm, Vector::Zr, 0);
    self.mac_to_ir(code);

    self.transform(code, Matrix::Lcm, Vector::Bk, 3);
    self.mac_to_ir(code);

    let r = ((self.gpr.rgbc.r as i32) << 4) * self.gpr.vector[3][0];
    let g = ((self.gpr.rgbc.g as i32) << 4) * self.gpr.vector[3][1];
    let b = ((self.gpr.rgbc.b as i32) << 4) * self.gpr.vector[3][2];

    self.depth_cue(code, r, g, b);
  }

  pub fn op_ncdt(&mut self, code: u32) {
    for i in 0..3 {
      self.transform(code, Matrix::Llm, Vector::Zr, i);
      self.mac_to_ir(code);

      self.transform(code, Matrix::Lcm, Vector::Bk, 3);
      self.mac_to_ir(code);

      let r = ((self.gpr.rgbc.r as i32) << 4) * self.gpr.vector[3][0];
      let g = ((self.gpr.rgbc.g as i32) << 4) * self.gpr.vector[3][1];
      let b = ((self.gpr.rgbc.b as i32) << 4) * self.gpr.vector[3][2];

      self.depth_cue(code, r, g, b);
    }
  }

  pub fn op_nclip(&mut self) {
    let temp = ((self.gpr.sx[0] as i64) * ((self.gpr.sy[1] - self.gpr.sy[2]) as i64))
      + ((self.gpr.sx[1] as i64) * ((self.gpr.sy[2] - self.gpr.sy[0]) as i64))
      + ((self.gpr.sx[2] as i64) * ((self.gpr.sy[0] - self.gpr.sy[1]) as i64));

    self.gpr.mac[0] = self.flag_f(temp) as i32;
  }

  pub fn op_ncs(&mut self, code: u32) {
    self.transform(code, Matrix::Llm, Vector::Zr, 0);
    self.mac_to_ir(code);

    self.transform(code, Matrix::Lcm, Vector::Bk, 3);
    self.mac_to_ir(code);
    self.mac_to_rgb();
  }

  pub fn op_nct(&mut self, code: u32) {
    for i in 0..3 {
      self.transform(code, Matrix::Llm, Vector::Zr, i);
      self.mac_to_ir(code);

      self.transform(code, Matrix::Lcm, Vector::Bk, 3);
      self.mac_to_ir(code);
      self.mac_to_rgb();
    }
  }

  pub fn op_op(&mut self, code: u32) {
    let matrix = &self.ccr.matrix[Matrix::Rot as usize];

    let shift = get_sf(code);

    self.gpr.mac[1] = ((matrix[1][1] * self.gpr.vector[3][2]) - (matrix[2][2] * self.gpr.vector[3][1])) >> shift;
    self.gpr.mac[2] = ((matrix[2][2] * self.gpr.vector[3][0]) - (matrix[0][0] * self.gpr.vector[3][2])) >> shift;
    self.gpr.mac[3] = ((matrix[0][0] * self.gpr.vector[3][1]) - (matrix[1][1] * self.gpr.vector[3][0])) >> shift;

    self.mac_to_ir(code);
  }

  pub fn op_rtps(&mut self, code: u32) {
    let div = self.transform_pt(code, Matrix::Rot, Vector::Tr, 0);

    self.transform_xy(div);
    self.transform_dq(div);
  }

  pub fn op_rtpt(&mut self, code: u32) {
    let mut div = 0;

    for i in 0..3 {
      div = self.transform_pt(code, Matrix::Rot, Vector::Tr, i);
      self.transform_xy(div);
    }

    self.transform_dq(div);
  }

  pub fn op_sqr(&mut self, code: u32) {
    let shift = get_sf(code);

    self.gpr.mac[1] = (self.gpr.vector[3][0] * self.gpr.vector[3][0]) >> shift;
    self.gpr.mac[2] = (self.gpr.vector[3][1] * self.gpr.vector[3][1]) >> shift;
    self.gpr.mac[3] = (self.gpr.vector[3][2] * self.gpr.vector[3][2]) >> shift;

    self.mac_to_ir(code);
  }
}

impl CoProcessor for GteCore {
  fn cop_is_present(&self) -> bool {
    true
  }

  fn cop_run(&mut self, code: u32) {
    self.run(code)
  }

  fn cop_read_gpr(&mut self, n: u32) -> u32 {
    self.read_gpr(n)
  }

  fn cop_write_gpr(&mut self, n: u32, val: u32) {
    self.write_gpr(n, val)
  }

  fn cop_read_ccr(&mut self, n: u32) -> u32 {
    self.read_ccr(n)
  }

  fn cop_write_ccr(&mut self, n: u32, val: u32) {
    self.write_ccr(n, val)
  }
}

fn get_sf(code: u32) -> u32 {
  if (code & (1 << 19)) != 0 {
    12
  } else {
    0
  }
}

fn get_mx(code: u32) -> Matrix {
  match (code >> 17) & 3 {
    0 => Matrix::Rot,
    1 => Matrix::Llm,
    2 => Matrix::Lcm,
    _ => Matrix::Nil,
  }
}

fn get_v(code: u32) -> i32 {
  ((code >> 15) & 3) as i32
}

fn get_cv(code: u32) -> Vector {
  match (code >> 13) & 3 {
    0 => Vector::Tr,
    1 => Vector::Bk,
    2 => Vector::Fc,
    _ => Vector::Zr,
  }
}

fn get_lm(code: u32) -> i32 {
  if (code & (1 << 10)) != 0 {
    0
  } else {
    -0x8000
  }
}

const A1_MAX: i32 = 30;
// const A2_MAX: i32 = 29;
// const A3_MAX: i32 = 28;
const A1_MIN: i32 = 27;
// const A2_MIN: i32 = 26;
// const A3_MIN: i32 = 25;
const B1: i32 = 24;
// const B2: i32 = 23;
// const B3: i32 = 22;
const C1: i32 = 21;
// const C2: i32 = 20;
// const C3: i32 = 19;
const D: i32 = 18;
const E: i32 = 17;
const F_MAX: i32 = 16;
const F_MIN: i32 = 15;
const G1: i32 = 14;
// const G2: i32 = 13;
const H: i32 = 12;

const UNR_TABLE: [u8; 0x101] = [
  0xFF, 0xFD, 0xFB, 0xF9, 0xF7, 0xF5, 0xF3, 0xF1, 0xEF, 0xEE, 0xEC, 0xEA, 0xE8, 0xE6, 0xE4, 0xE3, 0xE1, 0xDF, 0xDD, 0xDC, 0xDA,
  0xD8, 0xD6, 0xD5, 0xD3, 0xD1, 0xD0, 0xCE, 0xCD, 0xCB, 0xC9, 0xC8, 0xC6, 0xC5, 0xC3, 0xC1, 0xC0, 0xBE, 0xBD, 0xBB, 0xBA, 0xB8,
  0xB7, 0xB5, 0xB4, 0xB2, 0xB1, 0xB0, 0xAE, 0xAD, 0xAB, 0xAA, 0xA9, 0xA7, 0xA6, 0xA4, 0xA3, 0xA2, 0xA0, 0x9F, 0x9E, 0x9C, 0x9B,
  0x9A, 0x99, 0x97, 0x96, 0x95, 0x94, 0x92, 0x91, 0x90, 0x8F, 0x8D, 0x8C, 0x8B, 0x8A, 0x89, 0x87, 0x86, 0x85, 0x84, 0x83, 0x82,
  0x81, 0x7F, 0x7E, 0x7D, 0x7C, 0x7B, 0x7A, 0x79, 0x78, 0x77, 0x75, 0x74, 0x73, 0x72, 0x71, 0x70, 0x6F, 0x6E, 0x6D, 0x6C, 0x6B,
  0x6A, 0x69, 0x68, 0x67, 0x66, 0x65, 0x64, 0x63, 0x62, 0x61, 0x60, 0x5F, 0x5E, 0x5D, 0x5D, 0x5C, 0x5B, 0x5A, 0x59, 0x58, 0x57,
  0x56, 0x55, 0x54, 0x53, 0x53, 0x52, 0x51, 0x50, 0x4F, 0x4E, 0x4D, 0x4D, 0x4C, 0x4B, 0x4A, 0x49, 0x48, 0x48, 0x47, 0x46, 0x45,
  0x44, 0x43, 0x43, 0x42, 0x41, 0x40, 0x3F, 0x3F, 0x3E, 0x3D, 0x3C, 0x3C, 0x3B, 0x3A, 0x39, 0x39, 0x38, 0x37, 0x36, 0x36, 0x35,
  0x34, 0x33, 0x33, 0x32, 0x31, 0x31, 0x30, 0x2F, 0x2E, 0x2E, 0x2D, 0x2C, 0x2C, 0x2B, 0x2A, 0x2A, 0x29, 0x28, 0x28, 0x27, 0x26,
  0x26, 0x25, 0x24, 0x24, 0x23, 0x22, 0x22, 0x21, 0x20, 0x20, 0x1F, 0x1E, 0x1E, 0x1D, 0x1D, 0x1C, 0x1B, 0x1B, 0x1A, 0x19, 0x19,
  0x18, 0x18, 0x17, 0x16, 0x16, 0x15, 0x15, 0x14, 0x14, 0x13, 0x12, 0x12, 0x11, 0x11, 0x10, 0x0F, 0x0F, 0x0E, 0x0E, 0x0D, 0x0D,
  0x0C, 0x0C, 0x0B, 0x0A, 0x0A, 0x09, 0x09, 0x08, 0x08, 0x07, 0x07, 0x06, 0x06, 0x05, 0x05, 0x04, 0x04, 0x03, 0x03, 0x02, 0x02,
  0x01, 0x01, 0x00, 0x00, 0x00,
];
