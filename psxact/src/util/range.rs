pub fn between(val: u32, min: u32, max: u32) -> bool {
  return (val & !(min ^ max)) == min;
}
