use crate::{AddressWidth, IO};

pub struct MemCtl {
  biu: u32,
  exp1_base: u32,
  exp2_base: u32,
  exp1_conf: u32,
  exp2_conf: u32,
  exp3_conf: u32,
  bios_conf: u32,
  spu_conf: u32,
  cdc_conf: u32,
  time_conf: u32,
  wram_conf: u32,
}

impl MemCtl {
  pub fn new() -> MemCtl {
    MemCtl {
      biu: 0,
      exp1_base: 0,
      exp2_base: 0,
      exp1_conf: 0,
      exp2_conf: 0,
      exp3_conf: 0,
      bios_conf: 0,
      spu_conf: 0,
      cdc_conf: 0,
      time_conf: 0,
      wram_conf: 0,
    }
  }
}

impl IO for MemCtl {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    if width != AddressWidth::Word {
      panic!("")
    }

    match address {
      0x1F80_1000 => self.exp1_base,
      0x1F80_1004 => self.exp2_base,
      0x1F80_1008 => self.exp1_conf,
      0x1F80_100C => self.exp3_conf,
      0x1F80_1010 => self.bios_conf,
      0x1F80_1014 => self.spu_conf,
      0x1F80_1018 => self.cdc_conf,
      0x1F80_101C => self.exp2_conf,
      0x1F80_1020 => self.time_conf,
      0x1F80_1060 => self.wram_conf,
      0xFFFE_0130 => self.biu,
      _ => {
        panic!("")
      }
    }
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, data: u32) {
    if width != AddressWidth::Word {
      panic!("")
    }

    match address {
      0x1F80_1000 => {
        self.exp1_base = (data & 0x00FF_FFFF) | 0x1F00_0000;

        assert!(self.exp1_base == 0x1F00_0000);
      }
      0x1F80_1004 => {
        self.exp2_base = (data & 0x00FF_FFFF) | 0x1F00_0000;

        assert!(self.exp2_base == 0x1F80_2000);
      }
      0x1F80_1008 => {
        self.exp1_conf = data & 0xAF1F_FFFF;

        assert!(self.exp1_conf == 0x0013_243F);
      }
      0x1F80_100C => {
        self.exp3_conf = data & 0x2F1F_FFFF;

        assert!(self.exp3_conf == 0x0000_3022)
      }
      0x1F80_1010 => {
        self.bios_conf = data & 0x2F1F_FFFF;

        assert!(self.bios_conf == 0x0013_243F);
      }
      0x1F80_1014 => {
        self.spu_conf = data & 0x2F1F_FFFF;

        assert!(self.spu_conf == 0x2009_31E1 || self.spu_conf == 0x2209_31E1);
      }
      0x1F80_1018 => {
        self.cdc_conf = data & 0x2F1F_FFFF;

        assert!(
          self.cdc_conf == 0x0002_0843 || self.cdc_conf == 0x0002_0943 || self.cdc_conf == 0x2102_0843 // Oddworld - Abe's Oddysee
        );
      }
      0x1F80_101C => {
        self.exp2_conf = data & 0x2F1F_FFFF;

        assert!(self.exp2_conf == 0x0007_0777);
      }
      0x1F80_1020 => {
        self.time_conf = data & 0x0003_FFFF;

        assert!(
          self.time_conf == 0x0003_1125
            || self.time_conf == 0x0000_132C
            || self.time_conf == 0x0000_1323
            || self.time_conf == 0x0000_1325
        );
      }
      0x1F80_1060 => {
        self.wram_conf = data & 0xFFFF_FFFF;

        assert!(
          self.wram_conf == 0x0000_0B88 ||
          self.wram_conf == 0x0000_0888 // Metal Gear Solid
        );
      }
      0xFFFE_0130 => {
        // [0xFFFE_0130] = 0x0000_0804
        // [0xFFFE_0130] = 0x0000_0800
        // [0xFFFE_0130] = 0x0001_E988
        //
        //     17 :: nostr  - No Streaming
        //     16 :: ldsch  - Enable Load Scheduling
        //     15 :: bgnt   - Enable Bus Grant
        //     14 :: nopad  - No Wait State
        //     13 :: rdpri  - Enable Read Priority
        //     12 :: intp   - Interrupt Polarity
        //     11 :: is1    - Enable I-Cache Set 1
        //     10 :: is0    - Enable I-Cache Set 0
        //  9,  8 :: iblksz - I-Cache Refill Size
        //      7 :: ds     - Enable D-Cache
        //  5,  4 :: dblksz - D-Cache Refill Size
        //      3 :: ram    - Scratchpad RAM
        //      2 :: tag    - Tag Test Mode
        //      1 :: inv    - Invalidate Mode
        //      0 :: lock   - Lock Mode

        self.biu = data;

        assert!(self.biu == 0x0000_0804 || self.biu == 0x0000_0800 || self.biu == 0x0001_E988);
      }
      _ => {
        panic!("")
      }
    }
  }
}
