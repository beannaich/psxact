use crate::{timing::timing_add_cpu_time, AddressWidth, IO};

use super::MemoryBase;

pub const BIOS_SIZE: usize = 512 * 1024;
pub const BIOS_MASK: usize = BIOS_SIZE - 1;

pub struct Bios(MemoryBase);

impl Bios {
  pub fn new(path: &str) -> std::io::Result<Bios> {
    let file = MemoryBase::rom(path)?;
    let bios = Bios(file);

    Ok(bios)
  }

  pub fn patch(&mut self, address: u32, code: &[u32]) {
    for (n, &val) in code.iter().enumerate() {
      let offset = (n as u32) * 4;
      self.0.write_word(address + offset, val);
    }
  }

  pub fn patch_debug_tty(&mut self) {
    self.patch(
      0x6F0C,
      &[
        0x3401_0001, // li $at, 0x1
        0x0FF0_19E1, // jal 0xBFC0_6784
        0xAF81_A9C0, // sw $at -0x5460($gp)
      ],
    )
  }

  pub fn patch_skip_intro(&mut self) {
    self.patch(
      0x6990,
      &[
        0x0000_0000, // nop
      ],
    );
  }
}

impl IO for Bios {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    add_cpu_time(&width);

    self.0.io_read(width, address)
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    add_cpu_time(&width);

    self.0.io_write(width, address, val)
  }
}

fn add_cpu_time(width: &AddressWidth) {
  const ACCESS_1: i32 = 8;
  const ACCESS_N: i32 = 6;

  match width {
    AddressWidth::Byte => timing_add_cpu_time(ACCESS_1),
    AddressWidth::Half => timing_add_cpu_time(ACCESS_1 + (ACCESS_N * 1)),
    AddressWidth::Word => timing_add_cpu_time(ACCESS_1 + (ACCESS_N * 3)),
  }
}
