pub mod bios;
pub mod mem_ctl;
pub mod wram;

use crate::{AddressWidth, IO};

pub struct MemoryBase {
  pub data: Vec<u8>,
  pub mask: usize,
}

impl MemoryBase {
  pub fn ram(size: usize) -> MemoryBase {
    MemoryBase {
      mask: size - 1,
      data: vec![0; size],
    }
  }

  pub fn rom(path: &str) -> std::io::Result<MemoryBase> {
    let data = std::fs::read(path)?;
    Ok(MemoryBase {
      mask: data.len() - 1,
      data,
    })
  }

  #[inline]
  pub fn read_word(&self, address: u32) -> u32 {
    let idx = (address as usize) & self.mask & !3;
    let buf = &self.data[idx..idx + 4];

    ((buf[0] as u32) << (8 * 0)) |
    ((buf[1] as u32) << (8 * 1)) |
    ((buf[2] as u32) << (8 * 2)) |
    ((buf[3] as u32) << (8 * 3))
  }

  #[inline]
  pub fn read_half(&self, address: u32) -> u16 {
    let idx = (address as usize) & self.mask & !1;
    let buf = &self.data[idx..idx + 2];

    ((buf[0] as u16) << (8 * 0)) |
    ((buf[1] as u16) << (8 * 1))
  }

  #[inline]
  pub fn read_byte(&self, address: u32) -> u8 {
    self.data[(address as usize) & self.mask]
  }

  #[inline]
  pub fn write_word(&mut self, address: u32, val: u32) {
    let idx = (address as usize) & self.mask & !3;
    let buf = &mut self.data[idx..idx + 4];

    buf[0] = (val >> (8 * 0)) as u8;
    buf[1] = (val >> (8 * 1)) as u8;
    buf[2] = (val >> (8 * 2)) as u8;
    buf[3] = (val >> (8 * 3)) as u8;
  }

  #[inline]
  pub fn write_half(&mut self, address: u32, val: u16) {
    let idx = (address as usize) & self.mask & !1;
    let buf = &mut self.data[idx..idx + 4];

    buf[0] = (val >> (8 * 0)) as u8;
    buf[1] = (val >> (8 * 1)) as u8;
  }

  #[inline]
  pub fn write_byte(&mut self, address: u32, val: u8) {
    self.data[(address as usize) & self.mask] = val
  }
}

impl IO for MemoryBase {
  fn io_read(&mut self, width: AddressWidth, address: u32) -> u32 {
    match width {
      AddressWidth::Word => self.read_word(address),
      AddressWidth::Half => self.read_half(address) as u32,
      AddressWidth::Byte => self.read_byte(address) as u32,
    }
  }

  fn io_write(&mut self, width: AddressWidth, address: u32, val: u32) {
    match width {
      AddressWidth::Word => self.write_word(address, val),
      AddressWidth::Half => self.write_half(address, val as u16),
      AddressWidth::Byte => self.write_byte(address, val as u8),
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_can_write_byte() {
    let mut mem = MemoryBase::ram(256);
    mem.write_byte(0, 0);
  }
}
