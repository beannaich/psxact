use super::envelope::{EnvelopeDirection, EnvelopeMode, EnvelopeParams};

#[derive(Clone, Copy, PartialEq)]
pub enum AdsrState {
  Attack,
  Decay,
  Sustain,
  Release,
}

pub struct AdsrEnvelope {
  state: AdsrState,
  params: [EnvelopeParams; 4],

  divider: u16,

  level: i16,
  level_sustain: i16,
}

impl AdsrEnvelope {
  pub fn new() -> AdsrEnvelope {
    let mut envelope = AdsrEnvelope {
      state: AdsrState::Attack,
      params: [
        EnvelopeParams::new(),
        EnvelopeParams::new(),
        EnvelopeParams::new(),
        EnvelopeParams::new(),
      ],
      divider: 0,
      level: 0,
      level_sustain: 0,
    };

    envelope.set_config_lo(0);
    envelope.set_config_hi(0);
    envelope
  }

  pub fn step(&mut self) {
    let level = self.level;
    let param = self.get_current_params();
    let div_step = param.get_div_step(level);
    let lev_step = param.get_lev_step(level);

    self.divider += div_step;

    if self.divider >= 0x8000 {
      self.divider = 0; // TODO: should this be 'divider -= 0x8000'?
      self.level = (self.level as i64 + lev_step as i64).clamp(0x0000, 0x7FFF) as i16;

      if self.state == AdsrState::Attack && self.level == 0x7FFF {
        self.state = AdsrState::Decay;
      } else if self.state == AdsrState::Decay && self.level <= self.level_sustain {
        self.state = AdsrState::Sustain;
      }
    }
  }

  pub fn key_on(&mut self) {
    self.divider = 0;
    self.level = 0;
    self.state = AdsrState::Attack;
  }

  pub fn key_off(&mut self) {
    self.divider = 0;
    self.state = AdsrState::Release;
  }

  pub fn get_level(&self) -> i16 {
    self.level
  }

  pub fn set_level(&mut self, val: i16) {
    self.level = val
  }

  pub fn get_current_params(&mut self) -> &EnvelopeParams {
    let idx = self.state as usize;
    &self.params[idx]
  }

  pub fn set_config_lo(&mut self, val: u16) {
    let attack_mode = EnvelopeMode::from_u16((val >> 15) & 1);
    let attack_shift = ((val >> 10) & 31) as i32;
    let attack_step = (7 - ((val >> 8) & 3)) as i16;

    self.params[0].set_params(attack_mode, attack_shift, attack_step);

    if attack_mode == EnvelopeMode::Exponential {
      self.params[0].set_multi_phase(attack_step);
    }

    let decay_mode = EnvelopeMode::Exponential;
    let decay_shift = ((val >> 4) & 15) as i32;
    let decay_step = -8;

    self.params[1].set_params(decay_mode, decay_shift, decay_step);

    self.level_sustain = ((((val & 15) + 1) << 11) - 1) as i16; // 7FF..7FFF
  }

  pub fn set_config_hi(&mut self, val: u16) {
    let sustain_mode = EnvelopeMode::from_u16((val >> 15) & 1);
    let sustain_direction = EnvelopeDirection::from_u16((val >> 14) & 1);
    let sustain_shift = ((val >> 8) & 31) as i32;
    let sustain_step_raw = (7 - ((val >> 6) & 3)) as i16;
    let sustain_step;

    if sustain_direction == EnvelopeDirection::Decrease {
      sustain_step = !sustain_step_raw;
    } else {
      sustain_step = sustain_step_raw;
    }

    self.params[2].set_params(sustain_mode, sustain_shift, sustain_step);

    if sustain_mode == EnvelopeMode::Exponential && sustain_direction == EnvelopeDirection::Increase {
      self.params[2].set_multi_phase(sustain_step_raw);
    }

    let release_mode = EnvelopeMode::from_u16((val >> 5) & 1);
    let release_shift = (val & 31) as i32;
    let release_step = -8;

    self.params[3].set_params(release_mode, release_shift, release_step);
  }
}
