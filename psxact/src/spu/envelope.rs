#[derive(Clone, Copy, PartialEq)]
pub enum EnvelopeMode {
  Linear = 0,
  Exponential = 1,
  MultiPhase = 2,
}

impl EnvelopeMode {
  pub fn from_u16(val: u16) -> EnvelopeMode {
    match val & 1 {
      0 => EnvelopeMode::Linear,
      _ => EnvelopeMode::Exponential,
    }
  }
}

#[derive(PartialEq)]
pub enum EnvelopeDirection {
  Increase = 0,
  Decrease = 1,
}

impl EnvelopeDirection {
  pub fn from_u16(val: u16) -> EnvelopeDirection {
    match val & 1 {
      0 => EnvelopeDirection::Increase,
      _ => EnvelopeDirection::Decrease,
    }
  }
}

pub struct EnvelopeParams {
  mode: EnvelopeMode,

  div_step: u16,
  lev_step: i16,

  div_step_multi_phase: u16,
  lev_step_multi_phase: i16,
}

impl EnvelopeParams {
  pub fn new() -> EnvelopeParams {
    EnvelopeParams {
      mode: EnvelopeMode::Linear,
      div_step: 0,
      div_step_multi_phase: 0,
      lev_step: 0,
      lev_step_multi_phase: 0,
    }
  }

  pub fn get_div_step(&self, level: i16) -> u16 {
    if self.mode == EnvelopeMode::MultiPhase && level >= 0x6000 {
      return self.div_step_multi_phase;
    } else {
      return self.div_step;
    }
  }

  pub fn get_lev_step(&self, level: i16) -> i16 {
    match self.mode {
      EnvelopeMode::Linear => {
        return self.lev_step;
      }
      EnvelopeMode::Exponential => {
        let s: i32 = self.lev_step as i32;
        let l: i32 = level as i32;

        return ((s * l) >> 15) as i16;
      }
      EnvelopeMode::MultiPhase => {
        return if level >= 0x6000 {
          self.lev_step_multi_phase
        } else {
          self.lev_step
        }
      }
    }
  }

  pub fn set_params(&mut self, envelope_mode: EnvelopeMode, shift: i32, step: i16) {
    const TIME: u16 = 0x8000;

    self.mode = envelope_mode;
    self.lev_step = step << (11 - shift).clamp(0, 15);
    self.div_step = TIME >> (shift - 11).clamp(0, 15);
  }

  pub fn set_multi_phase(&mut self, step: i16) {
    self.mode = EnvelopeMode::MultiPhase;

    if step > 10 && self.div_step > 3 {
      self.div_step_multi_phase = self.div_step >> 2;
    } else if step >= 10 && self.div_step > 1 {
      self.div_step_multi_phase = self.div_step >> 1;
    } else {
      self.div_step_multi_phase = self.div_step;
    }

    if self.div_step_multi_phase == 0 {
      self.div_step_multi_phase = 1;
    }

    if step < 10 {
      self.lev_step_multi_phase = self.lev_step >> 2;
    } else if step == 10 {
      self.lev_step_multi_phase = self.lev_step >> 1;
    } else {
      self.lev_step_multi_phase = self.lev_step;
    }
  }
}
