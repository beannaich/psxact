pub struct SoundRamAddress;

impl SoundRamAddress {
  pub fn create(val: u16) -> u32 {
    (val as u32) * 4
  }
}

pub const SOUND_RAM_SIZE: usize = 256 * 1024;
pub const SOUND_RAM_MASK: usize = SOUND_RAM_SIZE - 1;

pub struct SoundRam {
  buffer: [u16; SOUND_RAM_SIZE],
}

impl SoundRam {
  pub fn new() -> SoundRam {
    SoundRam {
      buffer: [0; SOUND_RAM_SIZE],
    }
  }

  pub fn read(&self, address: u32) -> u16 {
    self.buffer[(address as usize) & SOUND_RAM_MASK]
  }

  pub fn write(&mut self, address: u32, data: u16) {
    self.buffer[(address as usize) & SOUND_RAM_MASK] = data
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_can_write() {
    let mut ram = SoundRam::new();
    ram.write(1024, 0)
  }
}
