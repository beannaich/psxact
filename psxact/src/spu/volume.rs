pub enum VolumeMode {
  Fixed = 0,
  Sweep = 1,
}

pub struct Volume {
  mode: VolumeMode,
  level: i16,
}

impl Volume {
  pub fn new() -> Volume {
    Volume {
      mode: VolumeMode::Fixed,
      level: 0,
    }
  }

  pub fn get_level(&self) -> i16 {
    self.level
  }

  pub fn set_level(&mut self, val: u16) {
    self.mode = match (val >> 15) & 1 {
      0 => VolumeMode::Fixed,
      _ => VolumeMode::Sweep,
    };

    if let VolumeMode::Fixed = self.mode {
      self.level = ((val << 1) & 0xFFFE) as i16;
    } else {
      panic!("Volume sweep isn't supported yet.");
    }
  }
}
