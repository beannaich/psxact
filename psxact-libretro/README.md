# psxact-libretro

This is the `libretro` core for the `psxact` emulator! Currently, this is very much alpha level.

Currently, the only requirement is that a `bios.rom` exists in the `CORE_ASSETS` directory.
